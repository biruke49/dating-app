import { MigrationInterface, QueryRunner } from "typeorm";

export class PreferenceMigrations1696975172641 implements MigrationInterface {
    name = 'PreferenceMigrations1696975172641'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "preferences" ("created_by" character varying, "updated_by" character varying, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "deleted_by" character varying, "archive_reason" text, "id" uuid NOT NULL DEFAULT uuid_generate_v4(), "customer_id" uuid NOT NULL, "minAge" integer NOT NULL DEFAULT '18', "maxAge" integer NOT NULL DEFAULT '99', "gender" character varying NOT NULL, "religion" character varying NOT NULL, "children" character varying NOT NULL, "verified" boolean NOT NULL, "location" character varying NOT NULL, CONSTRAINT "REL_ae4621dc8c81bb841dddf120b3" UNIQUE ("customer_id"), CONSTRAINT "PK_17f8855e4145192bbabd91a51be" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "preferences" ADD CONSTRAINT "FK_ae4621dc8c81bb841dddf120b37" FOREIGN KEY ("customer_id") REFERENCES "customers"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "preferences" DROP CONSTRAINT "FK_ae4621dc8c81bb841dddf120b37"`);
        await queryRunner.query(`DROP TABLE "preferences"`);
    }

}
