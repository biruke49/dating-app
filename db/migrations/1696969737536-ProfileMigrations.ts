import { MigrationInterface, QueryRunner } from "typeorm";

export class ProfileMigrations1696969737536 implements MigrationInterface {
    name = 'ProfileMigrations1696969737536'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "profiles" ("created_by" character varying, "updated_by" character varying, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "deleted_by" character varying, "archive_reason" text, "id" uuid NOT NULL DEFAULT uuid_generate_v4(), "customer_id" uuid NOT NULL, "bio" character varying, "interests" text, "education" character varying, "job" character varying, "height" integer, "body_type" character varying, "religion" character varying, "verified" boolean NOT NULL DEFAULT false, "enabled" boolean NOT NULL DEFAULT true, "notifyMe" boolean NOT NULL DEFAULT true, CONSTRAINT "REL_acf2dea7a30a1fb8dcc104dc4a" UNIQUE ("customer_id"), CONSTRAINT "PK_8e520eb4da7dc01d0e190447c8e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "customers" DROP COLUMN "bio"`);
        await queryRunner.query(`ALTER TABLE "profiles" ADD CONSTRAINT "FK_acf2dea7a30a1fb8dcc104dc4a1" FOREIGN KEY ("customer_id") REFERENCES "customers"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profiles" DROP CONSTRAINT "FK_acf2dea7a30a1fb8dcc104dc4a1"`);
        await queryRunner.query(`ALTER TABLE "customers" ADD "bio" character varying(255)`);
        await queryRunner.query(`DROP TABLE "profiles"`);
    }

}
