import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { ArchiveProfileCommand, CreateProfileCommand, UpdateProfileCommand } from '@customer/usecases/profile/profile.commands';
import { ProfileResponse } from '@customer/usecases/profile/profile.response';
import { ProfileCommands } from '@customer/usecases/profile/profile.usecase.commands';
import { ProfileQuery } from '@customer/usecases/profile/profile.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FileManagerService } from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('profiles')
@ApiTags('profiles')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class ProfileController {
    constructor(
        private command: ProfileCommands,
        private query: ProfileQuery,
        private readonly fileManagerService: FileManagerService,
      ) {}
      @Get('get-profile/:id')
      @ApiOkResponse({ type: ProfileResponse })
      async getProfile(@Param('id') id: string) {
        return this.query.getProfile(id);
      }
      @Get('get-archived-profile/:id')
      @ApiOkResponse({ type: ProfileResponse })
      async getArchivedProfile(@Param('id') id: string) {
        return this.query.getProfile(id, true);
      }
      @Get('get-profiles')
      @ApiPaginatedResponse(ProfileResponse)
      async getProfiles(@Query() query: CollectionQuery) {
        return this.query.getProfiles(query);
      }
      // @Get('group-profiles-by-status')
      // @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
      // async groupPassengersByStatus(@Query() query: CollectionQuery) {
      //   return this.userQuery.groupUsersByStatus(query);
      // }
      @Post('create-profile')
      //@AllowAnonymous()
    //   @UseGuards(PermissionsGuard('manage-profiles'))
      @ApiOkResponse({ type: ProfileResponse })
      async createProfile(
        @CurrentUser() user: UserInfo,
        @Body() createProfileCommand: CreateProfileCommand,
      ) {
        createProfileCommand.currentUser = user;
        return this.command.createProfile(createProfileCommand);
      }
      @Put('update-profile')
      @UseGuards(PermissionsGuard('manage-profiles'))
      @ApiOkResponse({ type: ProfileResponse })
      async updateProfile(
        @CurrentUser() user: UserInfo,
        @Body() updateProfileCommand: UpdateProfileCommand,
      ) {
        updateProfileCommand.currentUser = user;
        return this.command.updateProfile(updateProfileCommand);
      }
      @Delete('archive-profile')
      // @UseGuards(PermissionsGuard('manage-users'))
      @ApiOkResponse({ type: ProfileResponse })
      async archiveProfile(
        @CurrentUser() user: UserInfo,
        @Body() archiveCommand: ArchiveProfileCommand,
      ) {
        archiveCommand.currentUser = user;
        return this.command.archiveProfile(archiveCommand);
      }
      @Delete('delete-profile/:id')
      // @UseGuards(PermissionsGuard('manage-users'))
      @ApiOkResponse({ type: Boolean })
      async deleteProfile(@CurrentUser() user: UserInfo, @Param('id') id: string) {
        return this.command.deleteProfile(id, user);
      }
      @Post('restore-profile/:id')
      // @UseGuards(PermissionsGuard('manage-users'))
      @ApiOkResponse({ type: ProfileResponse })
      async restoreProfile(@CurrentUser() user: UserInfo, @Param('id') id: string) {
        return this.command.restoreProfile(id, user);
      }
      @Get('get-archived-profiles')
      @ApiPaginatedResponse(ProfileResponse)
      async getArchivedProfiles(@Query() query: CollectionQuery) {
        return this.query.getArchivedProfiles(query);
      }
      @Post('verify-profile/:id')
    //   @UseGuards(PermissionsGuard('activate-or-block-users'))
      @ApiOkResponse({ type: ProfileResponse })
      async VerifyProfile(
        @CurrentUser() user: UserInfo,
        @Param('id') id: string,
      ) {
        return this.command.verifyProfile(id, user);
      }
      // @Post('update-profile')
      // @ApiConsumes('multipart/form-data')
      // @UseInterceptors(
      //   FileInterceptor('profileImage', {
      //     storage: diskStorage({
      //       destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      //     }),
      //     fileFilter: (request, file, callback) => {
      //       if (!file.mimetype.includes('image')) {
      //         return callback(
      //           new BadRequestException('Provide a valid image'),
      //           false,
      //         );
      //       }
      //       callback(null, true);
      //     },
      //     limits: { fileSize: Math.pow(1024, 2) },
      //   }),
      // )
      // async addProfileImage(
      //   @CurrentUser() user: UserInfo,
      //   @UploadedFile() profileImage: Express.Multer.File,
      // ) {
      //   if (profileImage) {
      //     const result = await this.fileManagerService.uploadFile(
      //       profileImage,
      //       FileManagerHelper.UPLOADED_FILES_DESTINATION,
      //     );
      //     if (result) {
      //       return this.command.updateCustomerProfileImage(user.id, result);
      //     }
      //   }
      //   throw new BadRequestException(`Bad Request`);
      // }
      // @Post('update-profile-profile-image/:id')
      // @ApiConsumes('multipart/form-data')
      // @UseInterceptors(
      //   FileInterceptor('profileImage', {
      //     storage: diskStorage({
      //       destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      //     }),
      //     fileFilter: (request, file, callback) => {
      //       if (!file.mimetype.includes('image')) {
      //         return callback(
      //           new BadRequestException('Provide a valid image'),
      //           false,
      //         );
      //       }
      //       callback(null, true);
      //     },
      //     limits: { fileSize: Math.pow(1024, 2) },
      //   }),
      // )
      // async uploadProfileProfileImage(
      //   @Param('id') id: string,
      //   @UploadedFile() profileImage: Express.Multer.File,
      // ) {
      //   if (profileImage) {
      //     const result = await this.fileManagerService.uploadFile(
      //       profileImage,
      //       FileManagerHelper.UPLOADED_FILES_DESTINATION,
      //     );
      //     if (result) {
      //       return this.command.updateCustomerProfileImage(id, result);
      //     }
      //   }
      //   throw new BadRequestException(`Bad Request`);
      // }
}
