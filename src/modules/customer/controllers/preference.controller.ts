import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { ArchivePreferenceCommand, CreatePreferenceCommand, UpdatePreferenceCommand } from '@customer/usecases/preference/preference.commands';
import { PreferenceResponse } from '@customer/usecases/preference/preference.response';
import { PreferenceCommands } from '@customer/usecases/preference/preference.usecase.commands';
import { PreferenceQuery } from '@customer/usecases/preference/preference.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FileManagerService } from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('preferences')
@ApiTags('preferences')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class PreferenceController {
    constructor(
        private command: PreferenceCommands,
        private query: PreferenceQuery,
        private readonly fileManagerService: FileManagerService,
      ) {}
      @Get('get-preference/:id')
      @ApiOkResponse({ type: PreferenceResponse })
      async getPreference(@Param('id') id: string) {
        return this.query.getPreference(id);
      }
      @Get('get-archived-preference/:id')
      @ApiOkResponse({ type: PreferenceResponse })
      async getArchivedPreference(@Param('id') id: string) {
        return this.query.getPreference(id, true);
      }
      @Get('get-preferences')
      @ApiPaginatedResponse(PreferenceResponse)
      async getPreferences(@Query() query: CollectionQuery) {
        return this.query.getPreferences(query);
      }
      @Post('create-preference')
      @ApiOkResponse({ type: PreferenceResponse })
      async createPreference(
        @CurrentUser() user: UserInfo,
        @Body() createPreferenceCommand: CreatePreferenceCommand,
      ) {
        createPreferenceCommand.currentUser = user;
        return this.command.createPreference(createPreferenceCommand);
      }
      @Put('update-preference')
      @UseGuards(PermissionsGuard('manage-preferences'))
      @ApiOkResponse({ type: PreferenceResponse })
      async updatePreference(
        @CurrentUser() user: UserInfo,
        @Body() updatePreferenceCommand: UpdatePreferenceCommand,
      ) {
        updatePreferenceCommand.currentUser = user;
        return this.command.updatePreference(updatePreferenceCommand);
      }
      @Delete('archive-preference')
      // @UseGuards(PermissionsGuard('manage-users'))
      @ApiOkResponse({ type: PreferenceResponse })
      async archivePreference(
        @CurrentUser() user: UserInfo,
        @Body() archiveCommand: ArchivePreferenceCommand,
      ) {
        archiveCommand.currentUser = user;
        return this.command.archivePreference(archiveCommand);
      }
      @Delete('delete-preference/:id')
      // @UseGuards(PermissionsGuard('manage-preferences'))
      @ApiOkResponse({ type: Boolean })
      async deletePreference(@CurrentUser() user: UserInfo, @Param('id') id: string) {
        return this.command.deletePreference(id, user);
      }
      @Post('restore-preference/:id')
      // @UseGuards(PermissionsGuard('manage-preferences'))
      @ApiOkResponse({ type: PreferenceResponse })
      async restorePreference(@CurrentUser() user: UserInfo, @Param('id') id: string) {
        return this.command.restorePreference(id, user);
      }
      @Get('get-archived-preference')
      @ApiPaginatedResponse(PreferenceResponse)
      async getArchivedPreferences(@Query() query: CollectionQuery) {
        return this.query.getArchivedPreferences(query);
      }
}
