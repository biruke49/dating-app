import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { ArchiveCustomerCommand, CreateCustomerCommand, UpdateCustomerCommand } from '../usecases/customer/customer.commands';
import { CustomerResponse } from '../usecases/customer/customer.response';
import { CustomerCommands } from '../usecases/customer/customer.usecase.commands';
import { CustomerQuery } from '../usecases/customer/customer.usecase.queries';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FileManagerHelper, FileManagerService } from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { BadRequestException, Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiExtraModels, ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';

@Controller('customers')
@ApiTags('customers')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class CustomerController {
  constructor(
    private command: CustomerCommands,
    private customerQuery: CustomerQuery,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-customer/:id')
  @ApiOkResponse({ type: CustomerResponse })
  async getCustomer(@Param('id') id: string) {
    return this.customerQuery.getCustomer(id);
  }
  @Get('get-archived-customer/:id')
  @ApiOkResponse({ type: CustomerResponse })
  async getArchivedCustomer(@Param('id') id: string) {
    return this.customerQuery.getCustomer(id, true);
  }
  @Get('get-customers')
  @ApiPaginatedResponse(CustomerResponse)
  async getCustomers(@Query() query: CollectionQuery) {
    return this.customerQuery.getCustomers(query);
  }
  // @Get('group-users-by-status')
  // @ApiOkResponse({ type: GroupByStatusResponse, isArray: true })
  // async groupPassengersByStatus(@Query() query: CollectionQuery) {
  //   return this.userQuery.groupUsersByStatus(query);
  // }
  @Post('register-customer')
  @AllowAnonymous()
  @ApiOkResponse({ type: CustomerResponse })
  async registerCustomer(
    @Body() createUserCommand: CreateCustomerCommand,
  ) {
    return this.command.registerCustomer(createUserCommand);
  }
  @Post('create-customer')
  // @AllowAnonymous()
  @UseGuards(PermissionsGuard('manage-customers'))
  @ApiOkResponse({ type: CustomerResponse })
  async createCustomer(
    @CurrentUser() user: UserInfo,
    @Body() createUserCommand: CreateCustomerCommand,
  ) {
    createUserCommand.currentUser = user;
    return this.command.createCustomer(createUserCommand);
  }
  @Put('update-customer')
  @UseGuards(PermissionsGuard('manage-customers'))
  @ApiOkResponse({ type: CustomerResponse })
  async updateCustomer(
    @CurrentUser() user: UserInfo,
    @Body() updateUserCommand: UpdateCustomerCommand,
  ) {
    updateUserCommand.currentUser = user;
    return this.command.updateCustomer(updateUserCommand);
  }
  @Delete('archive-customer')
  // @UseGuards(PermissionsGuard('manage-customers'))
  @ApiOkResponse({ type: CustomerResponse })
  async archiveCustomer(
    @CurrentUser() user: UserInfo,
    @Body() archiveCommand: ArchiveCustomerCommand,
  ) {
    archiveCommand.currentUser = user;
    return this.command.archiveCustomer(archiveCommand);
  }
  @Delete('delete-customer/:id')
  // @UseGuards(PermissionsGuard('manage-customers'))
  @ApiOkResponse({ type: Boolean })
  async deleteCustomer(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteCustomer(id, user);
  }
  @Post('restore-customer/:id')
  // @UseGuards(PermissionsGuard('manage-customers'))
  @ApiOkResponse({ type: CustomerResponse })
  async restoreUser(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreCustomer(id, user);
  }
  @Get('get-archived-customers')
  @ApiPaginatedResponse(CustomerResponse)
  async getArchivedCustomers(@Query() query: CollectionQuery) {
    return this.customerQuery.getArchivedCustomers(query);
  }
  @Post('activate-or-block-customer/:id')
  @UseGuards(PermissionsGuard('activate-or-block-customers'))
  @ApiOkResponse({ type: CustomerResponse })
  async activateOrBlockCustomer(
    @CurrentUser() user: UserInfo,
    @Param('id') id: string,
  ) {
    return this.command.activateOrBlockCustomer(id, user);
  }
  @Post('update-profile')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  async addProfileImage(
    @CurrentUser() user: UserInfo,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        return this.command.updateCustomerProfileImage(user.id, result);
      }
    }
    throw new BadRequestException(`Bad Request`);
  }
  @Post('update-customer-profile-image/:id')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('profileImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  async uploadCustomerProfileImage(
    @Param('id') id: string,
    @UploadedFile() profileImage: Express.Multer.File,
  ) {
    if (profileImage) {
      const result = await this.fileManagerService.uploadFile(
        profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        return this.command.updateCustomerProfileImage(id, result);
      }
    }
    throw new BadRequestException(`Bad Request`);
  }
}
