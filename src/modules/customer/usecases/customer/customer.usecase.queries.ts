import { CustomerEntity } from '../../persistence/customer/customer.entity';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomerResponse } from './customer.response';
// import { GroupByStatusResponse } from '@libs/common/count-by-category.response';
@Injectable()
export class CustomerQuery {
  constructor(
    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>,
  ) {}
  async getCustomer(id: string, withDeleted = false): Promise<CustomerResponse> {
    const customer = await this.customerRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!customer[0]) {
      throw new NotFoundException(`Customer not found with id ${id}`);
    }
    return CustomerResponse.fromEntity(customer[0]);
  }
  async getCustomers(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CustomerResponse>> {
    const dataQuery = QueryConstructor.constructQuery<CustomerEntity>(
      this.customerRepository,
      query,
    );
    const d = new DataResponseFormat<CustomerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CustomerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getArchivedCustomers(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<CustomerResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<CustomerEntity>(
      this.customerRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<CustomerResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => CustomerResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  // async groupUsersByStatus(
  //   query: CollectionQuery,
  // ): Promise<GroupByStatusResponse[]> {
  //   query.select = [];
  //   query.select.push('enabled as enabled', 'COUNT(users.id)');
  //   const dataQuery = QueryConstructor.constructQuery<CustomerEntity>(
  //     this.userRepository,
  //     query,
  //   );
  //   const data = await dataQuery.getRawMany();
  //   const countResponses = [];
  //   data.map((d) => {
  //     const countResponse = new GroupByStatusResponse();
  //     countResponse.status = d.enabled;
  //     countResponse.count = d.count;
  //     countResponses.push(countResponse);
  //   });
  //   return countResponses;
  // }
}
