import { ApiProperty } from '@nestjs/swagger';
import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { EmergencyContact } from '@libs/common/emergency-contact';
import { CustomerEntity } from '../../persistence/customer/customer.entity';
import { Customer } from '../../domains/customer/customer';
import { ProfileResponse } from '../profile/profile.response';

export class CustomerResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  phoneNumber: string;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  enabled: boolean;
  @ApiProperty()
  profileImage: FileDto;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  emergencyContact: EmergencyContact;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  profile: ProfileResponse;
  static fromEntity(customerEntity: CustomerEntity): CustomerResponse {
    const customerResponse = new CustomerResponse();
    customerResponse.id = customerEntity.id;
    customerResponse.name = customerEntity.name;
    customerResponse.email = customerEntity.email;
    customerResponse.phoneNumber = customerEntity.phoneNumber;
    customerResponse.gender = customerEntity.gender;
    customerResponse.enabled = customerEntity.enabled;
    customerResponse.profileImage = customerEntity.profileImage;
    customerResponse.address = customerEntity.address;
    customerResponse.archiveReason = customerEntity.archiveReason;
    customerResponse.createdBy = customerEntity.createdBy;
    customerResponse.updatedBy = customerEntity.updatedBy;
    customerResponse.deletedBy = customerEntity.deletedBy;
    customerResponse.createdAt = customerEntity.createdAt;
    customerResponse.updatedAt = customerEntity.updatedAt;
    customerResponse.deletedAt = customerEntity.deletedAt;
    if (customerEntity.profile) {
      customerResponse.profile = ProfileResponse.fromEntity(
        customerEntity.profile,
      );
    }
    return customerResponse;
  }
  static fromDomain(customer: Customer): CustomerResponse {
    const customerResponse = new CustomerResponse();
    customerResponse.id = customer.id;
    customerResponse.name = customer.name;
    customerResponse.email = customer.email;
    customerResponse.phoneNumber = customer.phoneNumber;
    customerResponse.gender = customer.gender;
    customerResponse.enabled = customer.enabled;
    customerResponse.profileImage = customer.profileImage;
    customerResponse.address = customer.address;
    customerResponse.archiveReason = customer.archiveReason;
    customerResponse.createdBy = customer.createdBy;
    customerResponse.updatedBy = customer.updatedBy;
    customerResponse.deletedBy = customer.deletedBy;
    customerResponse.createdAt = customer.createdAt;
    customerResponse.updatedAt = customer.updatedAt;
    customerResponse.deletedAt = customer.deletedAt;
    if (customer.profile) {
      customerResponse.profile = ProfileResponse.fromDomain(customer.profile);
    }
    return customerResponse;
  }
}
