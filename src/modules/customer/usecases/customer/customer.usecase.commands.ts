import {
  FileManagerService,
  FileManagerHelper,
} from '@libs/common/file-manager';
import { FileDto } from '@libs/common/file-dto';
import { FileResponseDto } from '@libs/common/file-manager/dtos/file-response.dto';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { CredentialType } from '@libs/common/enums';
import { CreateAccountCommand } from '@account/usecases/accounts/account.commands';
import { AccountCommands } from '@account/usecases/accounts/account.usecase.commands';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserInfo } from '@account/dtos/user-info.dto';
import { Util } from '@libs/common/util';
import { CustomerRepository } from '../../persistence/customer/customer.repository';
import {
  ArchiveCustomerCommand,
  CreateCustomerCommand,
  UpdateCustomerCommand,
} from './customer.commands';
import { CustomerResponse } from './customer.response';
@Injectable()
export class CustomerCommands {
  constructor(
    private customerRepository: CustomerRepository,
    private readonly accountCommands: AccountCommands,
    private eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createCustomer(
    command: CreateCustomerCommand,
  ): Promise<CustomerResponse> {
    if (
      await this.customerRepository.getByPhoneNumber(command.phoneNumber, true)
    ) {
      throw new BadRequestException(
        `Customer already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.customerRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Customer already exist with this email Address`,
      );
    }
    const customerDomain = CreateCustomerCommand.fromCommand(command);
    const customer = await this.customerRepository.insert(customerDomain);
    if (customer) {
      const password = `P@ssw0rd`; // Util.generatePassword(9); //`P@ssw0rd`;
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = customer.id;
      createAccountCommand.type = CredentialType.Customer;
      createAccountCommand.isActive = true;
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      createAccountCommand.password = Util.hashPassword(password);
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      if (account && account.email) {
        this.eventEmitter.emit('send.verification.code', {
          phoneNumber: account.phoneNumber,
        });
      }

      this.eventEmitter.emit('activity-logger.store', {
        modelId: customer.id,
        modelName: MODELS.CUSTOMER,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return CustomerResponse.fromDomain(customer);
  }
  async registerCustomer(
    command: CreateCustomerCommand,
  ): Promise<CustomerResponse> {
    if (
      await this.customerRepository.getByPhoneNumber(command.phoneNumber, true)
    ) {
      throw new BadRequestException(
        `Customer already exist with this phone number`,
      );
    }
    if (
      command.email &&
      (await this.customerRepository.getByEmail(command.email, true))
    ) {
      throw new BadRequestException(
        `Customer already exist with this email Address`,
      );
    }
    const customerDomain = CreateCustomerCommand.fromCommand(command);
    const customer = await this.customerRepository.insert(customerDomain);
    if (customer) {
      const password = `P@ssw0rd`; // Util.generatePassword(9); //`P@ssw0rd`;
      const createAccountCommand = new CreateAccountCommand();
      createAccountCommand.email = command.email;
      createAccountCommand.phoneNumber = command.phoneNumber;
      createAccountCommand.name = command.name;
      createAccountCommand.accountId = customer.id;
      createAccountCommand.type = CredentialType.Customer;
      createAccountCommand.isActive = true;
      createAccountCommand.address = command.address;
      createAccountCommand.gender = command.gender;
      createAccountCommand.password = Util.hashPassword(password);
      const account = await this.accountCommands.createAccount(
        createAccountCommand,
      );
      if (account && account.email) {
        this.eventEmitter.emit('send.verification.code', {
          phoneNumber: account.phoneNumber,
        });
      }

      this.eventEmitter.emit('activity-logger.store', {
        modelId: customer.id,
        modelName: MODELS.CUSTOMER,
        action: ACTIONS.CREATE,
        userId: account.id,
        user: account,
      });
    }
    return CustomerResponse.fromDomain(customer);
  }
  async updateCustomer(
    command: UpdateCustomerCommand,
  ): Promise<CustomerResponse> {
    const customerDomain = await this.customerRepository.getById(command.id);
    if (!customerDomain) {
      throw new NotFoundException(`Customer not found with id ${command.id}`);
    }
    const oldPayload = customerDomain;
    customerDomain.email = command.email;
    customerDomain.name = command.name;
    customerDomain.address = command.address;
    customerDomain.username = command.username;
    customerDomain.gender = command.gender;
    customerDomain.dateOfBirth = command.dateOfBirth;
    customerDomain.phoneNumber = command.phoneNumber;
    customerDomain.gender = command.gender;
    const customer = await this.customerRepository.update(customerDomain);
    if (customer) {
      this.eventEmitter.emit('update.account', {
        accountId: customer.id,
        name: customer.name,
        email: customer.email,
        type: CredentialType.Customer,
        phoneNumber: customer.phoneNumber,
        address: customer.address,
        gender: customer.gender,
        profileImage: customer.profileImage,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: customer.id,
        modelName: MODELS.CUSTOMER,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: oldPayload,
        payload: customer,
      });
    }
    return CustomerResponse.fromDomain(customer);
  }
  async archiveCustomer(
    command: ArchiveCustomerCommand,
  ): Promise<CustomerResponse> {
    const customerDomain = await this.customerRepository.getById(command.id);
    if (!customerDomain) {
      throw new NotFoundException(`Customer not found with id ${command.id}`);
    }
    customerDomain.deletedAt = new Date();
    customerDomain.deletedBy = command.currentUser.id;
    customerDomain.archiveReason = command.reason;
    const result = await this.customerRepository.update(customerDomain);
    if (result) {
      this.eventEmitter.emit('account.archived', {
        phoneNumber: customerDomain.email,
        id: customerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.CUSTOMER,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return CustomerResponse.fromDomain(result);
  }
  async restoreCustomer(
    id: string,
    currentUser: UserInfo,
  ): Promise<CustomerResponse> {
    const customerDomain = await this.customerRepository.getById(id, true);
    if (!customerDomain) {
      throw new NotFoundException(`Customer not found with id ${id}`);
    }
    const r = await this.customerRepository.restore(id);
    if (r) {
      customerDomain.deletedAt = null;
      this.eventEmitter.emit('account.restored', {
        phoneNumber: customerDomain.email,
        id: customerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CUSTOMER,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return CustomerResponse.fromDomain(customerDomain);
  }
  async deleteCustomer(id: string, currentUser: UserInfo): Promise<boolean> {
    const customerDomain = await this.customerRepository.getById(id, true);
    if (!customerDomain) {
      throw new NotFoundException(`Customer not found with id ${id}`);
    }
    const result = await this.customerRepository.delete(id);
    if (result) {
      if (customerDomain.profileImage) {
        await this.fileManagerService.removeFile(
          customerDomain.profileImage,
          FileManagerHelper.UPLOADED_FILES_DESTINATION,
        );
      }
      this.eventEmitter.emit('account.deleted', {
        phoneNumber: customerDomain.email,
        id: customerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CUSTOMER,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
  async activateOrBlockCustomer(
    id: string,
    currentUser: UserInfo,
  ): Promise<CustomerResponse> {
    const customerDomain = await this.customerRepository.getById(id);
    if (!customerDomain) {
      throw new NotFoundException(`Customer not found with id ${id}`);
    }
    customerDomain.enabled = !customerDomain.enabled;
    const result = await this.customerRepository.update(customerDomain);
    if (result) {
      this.eventEmitter.emit('account.activate-or-block', {
        phoneNumber: customerDomain.email,
        id: customerDomain.id,
      });
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.CUSTOMER,
        action: customerDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return CustomerResponse.fromDomain(result);
  }
  async updateCustomerProfileImage(id: string, fileDto: FileResponseDto) {
    const customerDomain = await this.customerRepository.getById(id, true);
    if (!customerDomain) {
      throw new NotFoundException(`Customer not found with id ${id}`);
    }
    if (customerDomain.profileImage && fileDto) {
      await this.fileManagerService.removeFile(
        customerDomain.profileImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    customerDomain.profileImage = fileDto as FileDto;
    const result = await this.customerRepository.update(customerDomain);
    if (result) {
      this.eventEmitter.emit('update-account-profile', {
        id: result.id,
        profileImage: result.profileImage,
      });
    }
    return CustomerResponse.fromDomain(result);
  }
}
