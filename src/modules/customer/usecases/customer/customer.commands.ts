import { UserInfo } from '@account/dtos/user-info.dto';
import { Address } from '@libs/common/address';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty } from 'class-validator';
import { Gender } from '@libs/common/enums';
import { Customer } from '@customer/domains/customer/customer';

export class CreateCustomerCommand {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  @IsEmail()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  username: string;
    @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty({
    enum: Gender,
  })
  @IsEnum(Gender, {
    message: 'Customer Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  @IsNotEmpty()
  address: Address;
  @ApiProperty()
  @IsNotEmpty()
  dateOfBirth: Date;
  currentUser: UserInfo;

  static fromCommand(command: CreateCustomerCommand): Customer {
    const customerDomain = new Customer();
    customerDomain.name = command.name;
    customerDomain.email = command.email;
    customerDomain.username = command.username;
    customerDomain.phoneNumber = command.phoneNumber;
    customerDomain.gender = command.gender;
    customerDomain.dateOfBirth = command.dateOfBirth;
    customerDomain.address = command.address;
    return customerDomain;
  }
}
export class UpdateCustomerCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  phoneNumber: string;
  @ApiProperty()
  @IsNotEmpty()
  username: string;
  @ApiProperty({
    example: 'someone@gmail.com',
  })
  @IsEmail()
  email: string;
  @ApiProperty()
  @IsEnum(Gender, {
    message: 'Customer Gender must be either male or female',
  })
  gender: string;
  @ApiProperty()
  address: Address;
  @ApiProperty()
  dateOfBirth: Date;
  currentUser: UserInfo;
}
export class ArchiveCustomerCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
