import {
  FileManagerService,
} from '@libs/common/file-manager';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';

import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserInfo } from '@account/dtos/user-info.dto';
import {
  ArchiveProfileCommand,
  CreateProfileCommand,
  UpdateProfileCommand,
} from './profile.commands';
import { ProfileResponse } from './profile.response';
import { ProfileRepository } from '@customer/persistence/profile/profile.repository';
@Injectable()
export class ProfileCommands {
  constructor(
    private profileRepository: ProfileRepository,
    private eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createProfile(command: CreateProfileCommand): Promise<ProfileResponse> {
    const profileDomain = CreateProfileCommand.fromCommand(command);
    const profile = await this.profileRepository.insert(profileDomain);
    if (profile) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: profile.id,
        modelName: MODELS.PROFILE,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return ProfileResponse.fromDomain(profile);
  }
  async updateProfile(command: UpdateProfileCommand): Promise<ProfileResponse> {
    const profileDomain = await this.profileRepository.getByCustomerId(
      command.id,
    );
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${command.id}`);
    }
    const oldPayload = profileDomain;
    profileDomain.customerId = command.customerId;
    profileDomain.bio = command.bio;
    profileDomain.interests = command.interests;
    profileDomain.education = command.education;
    profileDomain.job = command.job;
    profileDomain.bodyType = command.bodyType;
    profileDomain.religion = command.religion;
    profileDomain.height = command.height;
    const profile = await this.profileRepository.update(profileDomain);
    if (profile) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: profile.id,
        modelName: MODELS.PROFILE,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: oldPayload,
        payload: profile,
      });
    }
    return ProfileResponse.fromDomain(profile);
  }
  async verifyProfile(
    id: string,
    currentUser: UserInfo,
  ): Promise<ProfileResponse> {
    const profileDomain = await this.profileRepository.getByCustomerId(id);
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${id}`);
    }
    profileDomain.verified = !profileDomain.verified;
    const result = await this.profileRepository.update(profileDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PROFILE,
        action: profileDomain.verified ? ACTIONS.VERIFY : ACTIONS.NOTVERIFY,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return ProfileResponse.fromDomain(result);
  }
  async actiateOrDeactivateProfile(
    id: string,
    currentUser: UserInfo,
  ): Promise<ProfileResponse> {
    const profileDomain = await this.profileRepository.getByCustomerId(id);
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${id}`);
    }
    profileDomain.enabled = !profileDomain.enabled;
    const result = await this.profileRepository.update(profileDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PROFILE,
        action: profileDomain.enabled ? ACTIONS.ACTIVATE : ACTIONS.BLOCK,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return ProfileResponse.fromDomain(result);
  }
  async NotifyOrNotNotifyProfile(
    id: string,
    currentUser: UserInfo,
  ): Promise<ProfileResponse> {
    const profileDomain = await this.profileRepository.getByCustomerId(id);
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${id}`);
    }
    profileDomain.notifyMe = !profileDomain.notifyMe;
    const result = await this.profileRepository.update(profileDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PROFILE,
        action: profileDomain.notifyMe ? ACTIONS.NOTIFIY : ACTIONS.NOTNOTIFY,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return ProfileResponse.fromDomain(result);
  }
  // async updateCustomerProfileImage(id: string, fileDto: FileResponseDto) {
  //   const profileDomain = await this.profileRepository.getByCustomerId(
  //     id,
  //     true,
  //   );
  //   if (!profileDomain) {
  //     throw new NotFoundException(`Profile not found with id ${id}`);
  //   }
  //   if (profileDomain.profileImage && fileDto) {
  //     await this.fileManagerService.removeFile(
  //       profileDomain.profileImage,
  //       FileManagerHelper.UPLOADED_FILES_DESTINATION,
  //     );
  //   }
  //   profileDomain.profileImage = fileDto as FileDto;
  //   const result = await this.profileRepository.update(profileDomain);
  //   if (result) {
  //     this.eventEmitter.emit('update-account-profile', {
  //       id: result.id,
  //       profileImage: result.profileImage,
  //     });
  //   }
  //   return ProfileResponse.fromDomain(result);
  // }
  async archiveProfile(
    command: ArchiveProfileCommand,
  ): Promise<ProfileResponse> {
    const profileDomain = await this.profileRepository.getByCustomerId(command.id);
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${command.id}`);
    }
    profileDomain.deletedAt = new Date();
    profileDomain.deletedBy = command.currentUser.id;
    const result = await this.profileRepository.update(profileDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.PROFILE,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return ProfileResponse.fromDomain(result);
  }
  async restoreProfile(
    id: string,
    currentUser: UserInfo,
  ): Promise<ProfileResponse> {
    const profileDomain = await this.profileRepository.getByCustomerId(
      id,
      true,
    );
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${id}`);
    }
    const r = await this.profileRepository.restore(id);
    if (r) {
      profileDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PROFILE,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return ProfileResponse.fromDomain(profileDomain);
  }
  async deleteProfile(id: string, currentUser: UserInfo): Promise<boolean> {
    const profileDomain = await this.profileRepository.getByCustomerId(id, true);
    if (!profileDomain) {
      throw new NotFoundException(`Profile not found with id ${id}`);
    }
    const result = await this.profileRepository.delete(id);
    if (result) {
      // if (profileDomain.profileImage) {
      //   await this.fileManagerService.removeFile(
      //     profileDomain.profileImage,
      //     FileManagerHelper.UPLOADED_FILES_DESTINATION,
      //   );
      // }
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.USER,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
}
