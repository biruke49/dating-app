import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProfileResponse } from './profile.response';
import { ProfileEntity } from '@customer/persistence/profile/profile.entity';
// import { GroupByStatusResponse } from '@libs/common/count-by-category.response';
@Injectable()
export class ProfileQuery {
  constructor(
    @InjectRepository(ProfileEntity)
    private profileRepository: Repository<ProfileEntity>,
  ) {}
  async getProfile(id: string, withDeleted = false): Promise<ProfileResponse> {
    const profile = await this.profileRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!profile[0]) {
      throw new NotFoundException(`Profile not found with id ${id}`);
    }
    return ProfileResponse.fromEntity(profile[0]);
  }
  async getProfileByCustomerId(
    customerId: string,
    withDeleted = false,
  ): Promise<ProfileResponse> {
    const profile = await this.profileRepository.find({
      where: { customerId: customerId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!profile[0]) {
      throw new NotFoundException(
        `Profile not found for customer with id ${customerId}`,
      );
    }
    return ProfileResponse.fromEntity(profile[0]);
  }
  async getProfiles(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ProfileResponse>> {
    const dataQuery = QueryConstructor.constructQuery<ProfileEntity>(
      this.profileRepository,
      query,
    );
    const d = new DataResponseFormat<ProfileResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ProfileResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
    async getArchivedProfiles(
      query: CollectionQuery,
    ): Promise<DataResponseFormat<ProfileResponse>> {
      if (!query.filter) {
        query.filter = [];
      }
      query.filter.push([
        {
          field: 'deleted_at',
          operator: FilterOperators.NotNull,
        },
      ]);
      const dataQuery = QueryConstructor.constructQuery<ProfileEntity>(
        this.profileRepository,
        query,
      );
      dataQuery.withDeleted();
      const d = new DataResponseFormat<ProfileResponse>();
      if (query.count) {
        d.count = await dataQuery.getCount();
      } else {
        const [result, total] = await dataQuery.getManyAndCount();
        d.data = result.map((entity) => ProfileResponse.fromEntity(entity));
        d.count = total;
      }
      return d;
    }
  // async groupUsersByStatus(
  //   query: CollectionQuery,
  // ): Promise<GroupByStatusResponse[]> {
  //   query.select = [];
  //   query.select.push('enabled as enabled', 'COUNT(users.id)');
  //   const dataQuery = QueryConstructor.constructQuery<UserEntity>(
  //     this.userRepository,
  //     query,
  //   );
  //   const data = await dataQuery.getRawMany();
  //   const countResponses = [];
  //   data.map((d) => {
  //     const countResponse = new GroupByStatusResponse();
  //     countResponse.status = d.enabled;
  //     countResponse.count = d.count;
  //     countResponses.push(countResponse);
  //   });
  //   return countResponses;
  // }
}
