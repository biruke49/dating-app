import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Profile } from '@customer/domains/profile/profile';

export class CreateProfileCommand {
  @ApiProperty()
  customerId: string;
  @ApiProperty()
  bio: string;
  @ApiProperty()
  interests: string[];
  @ApiProperty()
  education: string;
  @ApiProperty()
  job: string;
  @ApiProperty()
  height: number;
  @ApiProperty()
  bodyType: string;
  @ApiProperty()
  religion: string;
  currentUser: UserInfo;

  static fromCommand(command: CreateProfileCommand): Profile {
    const customerDomain = new Profile();
    customerDomain.customerId = command.customerId;
    customerDomain.bio = command.bio;
    customerDomain.interests = command.interests;
    customerDomain.education = command.education;
    customerDomain.job = command.job;
    customerDomain.height = command.height;
    customerDomain.bodyType = command.bodyType;
    customerDomain.religion = command.religion;
    return customerDomain;
  }
}
export class UpdateProfileCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  customerId: string;
  @ApiProperty()
  bio: string;
  @ApiProperty()
  interests: string[];
  @ApiProperty()
  education: string;
  @ApiProperty()
  job: string;
  @ApiProperty()
  height: number;
  @ApiProperty()
  bodyType: string;
  @ApiProperty()
  religion: string;
  enabled: string;
  verified: string;
  currentUser: UserInfo;
}
export class ArchiveProfileCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
