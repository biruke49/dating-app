import { ApiProperty } from '@nestjs/swagger';
import { ProfileEntity } from '@customer/persistence/profile/profile.entity';
import { Profile } from '@customer/domains/profile/profile';

export class ProfileResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  customerId: string;
  @ApiProperty()
  bio: string;
  @ApiProperty()
  interests: string[];
  @ApiProperty()
  education: string;
  @ApiProperty()
  job: string;
  @ApiProperty()
  height: number;
  @ApiProperty()
  bodyType: string;
  @ApiProperty()
  religion: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  static fromEntity(profileEntity: ProfileEntity): ProfileResponse {
    const profileResponse = new ProfileResponse();
    profileResponse.id = profileEntity.id;
    profileResponse.customerId = profileEntity.customerId;
    profileResponse.bio = profileEntity.bio;
    profileResponse.interests = profileEntity.interests;
    profileResponse.education = profileEntity.education;
    profileResponse.job = profileEntity.job;
    profileResponse.height = profileEntity.height;
    profileResponse.bodyType = profileEntity.bodyType;
    profileResponse.religion = profileEntity.religion;
    profileResponse.createdBy = profileEntity.createdBy;
    profileResponse.updatedBy = profileEntity.updatedBy;
    profileResponse.deletedBy = profileEntity.deletedBy;
    profileResponse.createdAt = profileEntity.createdAt;
    profileResponse.updatedAt = profileEntity.updatedAt;
    profileResponse.deletedAt = profileEntity.deletedAt;
    return profileResponse;
  }
  static fromDomain(profile: Profile): ProfileResponse {
    const profileResponse = new ProfileResponse();
    profileResponse.id = profile.id;
    profileResponse.bio = profile.bio;
    profileResponse.interests = profile.interests;
    profileResponse.education = profile.education;
    profileResponse.job = profile.job;
    profileResponse.height = profile.height;
    profileResponse.bodyType = profile.bodyType;
    profileResponse.religion = profile.religion;
    profileResponse.createdBy = profile.createdBy;
    profileResponse.updatedBy = profile.updatedBy;
    profileResponse.deletedBy = profile.deletedBy;
    profileResponse.createdAt = profile.createdAt;
    profileResponse.updatedAt = profile.updatedAt;
    profileResponse.deletedAt = profile.deletedAt;
    return profileResponse;
  }
}
