import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Preference } from '@customer/domains/preference/preference';

export class CreatePreferenceCommand {
  @ApiProperty()
  customerId: string;
  @ApiProperty()
  minAge: number;
  @ApiProperty()
  maxAge: number;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  children: string;
  @ApiProperty()
  verified: boolean;
  @ApiProperty()
  location: string;
  @ApiProperty()
  religion: string;
  currentUser: UserInfo;

  static fromCommand(command: CreatePreferenceCommand): Preference {
    const customerDomain = new Preference();
    customerDomain.customerId = command.customerId;
    customerDomain.minAge = command.minAge;
    customerDomain.maxAge = command.maxAge;
    customerDomain.gender = command.gender;
    customerDomain.children = command.children;
    customerDomain.verified = command.verified;
    customerDomain.location = command.location;
    customerDomain.religion = command.religion;
    return customerDomain;
  }
}
export class UpdatePreferenceCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  customerId: string;
  @ApiProperty()
  minAge: number;
  @ApiProperty()
  maxAge: number;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  children: string;
  @ApiProperty()
  verified: boolean;
  @ApiProperty()
  location: string;
  @ApiProperty()
  religion: string;
  enabled: string;
  currentUser: UserInfo;
}
export class ArchivePreferenceCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
