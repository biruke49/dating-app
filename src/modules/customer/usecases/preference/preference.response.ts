import { Preference } from '@customer/domains/preference/preference';
import { PreferenceEntity } from '@customer/persistence/preference/preference.entity';
import { ApiProperty } from '@nestjs/swagger';

export class PreferenceResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  customerId: string;
  @ApiProperty()
  minAge: number;
  @ApiProperty()
  maxAge: number;
  @ApiProperty()
  gender: string;
  @ApiProperty()
  children: string;
  @ApiProperty()
  verified: boolean;
  @ApiProperty()
  location: string;
  @ApiProperty()
  religion: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  static fromEntity(preferenceEntity: PreferenceEntity): PreferenceResponse {
    const preferenceResponse = new PreferenceResponse();
    preferenceResponse.id = preferenceEntity.id;
    preferenceResponse.customerId = preferenceEntity.customerId;
    preferenceResponse.minAge = preferenceEntity.minAge;
    preferenceResponse.maxAge = preferenceEntity.maxAge;
    preferenceResponse.gender = preferenceEntity.gender;
    preferenceResponse.children = preferenceEntity.children;
    preferenceResponse.verified = preferenceEntity.verified;
    preferenceResponse.location = preferenceEntity.location;
    preferenceResponse.religion = preferenceEntity.religion;
    preferenceResponse.createdBy = preferenceEntity.createdBy;
    preferenceResponse.updatedBy = preferenceEntity.updatedBy;
    preferenceResponse.deletedBy = preferenceEntity.deletedBy;
    preferenceResponse.createdAt = preferenceEntity.createdAt;
    preferenceResponse.updatedAt = preferenceEntity.updatedAt;
    preferenceResponse.deletedAt = preferenceEntity.deletedAt;
    return preferenceResponse;
  }
  static fromDomain(preference: Preference): PreferenceResponse {
    const preferenceResponse = new PreferenceResponse();
    preferenceResponse.id = preference.id;
    preferenceResponse.minAge = preference.minAge;
    preferenceResponse.maxAge = preference.maxAge;
    preferenceResponse.gender = preference.gender;
    preferenceResponse.children = preference.children;
    preferenceResponse.verified = preference.verified;
    preferenceResponse.location = preference.location;
    preferenceResponse.religion = preference.religion;
    preferenceResponse.createdBy = preference.createdBy;
    preferenceResponse.updatedBy = preference.updatedBy;
    preferenceResponse.deletedBy = preference.deletedBy;
    preferenceResponse.createdAt = preference.createdAt;
    preferenceResponse.updatedAt = preference.updatedAt;
    preferenceResponse.deletedAt = preference.deletedAt;
    return preferenceResponse;
  }
}
