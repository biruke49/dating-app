import { FileManagerService } from '@libs/common/file-manager';
import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';

import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PreferenceRepository } from '@customer/persistence/preference/preference.repository';
import {
  ArchivePreferenceCommand,
  CreatePreferenceCommand,
  UpdatePreferenceCommand,
} from './preference.commands';
import { PreferenceResponse } from './preference.response';

@Injectable()
export class PreferenceCommands {
  constructor(
    private preferenceRepository: PreferenceRepository,
    private eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createPreference(
    command: CreatePreferenceCommand,
  ): Promise<PreferenceResponse> {
    const preferenceDomain = CreatePreferenceCommand.fromCommand(command);
    const preference = await this.preferenceRepository.insert(preferenceDomain);
    if (preference) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: preference.id,
        modelName: MODELS.PREFERENCE,
        action: ACTIONS.CREATE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return PreferenceResponse.fromDomain(preference);
  }
  async updatePreference(
    command: UpdatePreferenceCommand,
  ): Promise<PreferenceResponse> {
    const preferenceDomain = await this.preferenceRepository.getByPreferenceId(
      command.id,
    );
    if (!preferenceDomain) {
      throw new NotFoundException(`Preference not found with id ${command.id}`);
    }
    const oldPayload = preferenceDomain;
    preferenceDomain.customerId = command.customerId;
    preferenceDomain.minAge = command.minAge;
    preferenceDomain.maxAge = command.maxAge;
    preferenceDomain.gender = command.gender;
    preferenceDomain.verified = command.verified;
    preferenceDomain.location = command.location;
    preferenceDomain.religion = command.religion;
    preferenceDomain.children = command.children;
    const preference = await this.preferenceRepository.update(preferenceDomain);
    if (preference) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: preference.id,
        modelName: MODELS.PREFERENCE,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        oldPayload: oldPayload,
        payload: preference,
      });
    }
    return PreferenceResponse.fromDomain(preference);
  }
  async archivePreference(
    command: ArchivePreferenceCommand,
  ): Promise<PreferenceResponse> {
    const preferenceDomain = await this.preferenceRepository.getByPreferenceId(
      command.id,
    );
    if (!preferenceDomain) {
      throw new NotFoundException(`Preference not found with id ${command.id}`);
    }
    preferenceDomain.deletedAt = new Date();
    preferenceDomain.deletedBy = command.currentUser.id;
    const result = await this.preferenceRepository.update(preferenceDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.PREFERENCE,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return PreferenceResponse.fromDomain(result);
  }
  async restorePreference(
    id: string,
    currentUser: UserInfo,
  ): Promise<PreferenceResponse> {
    const preferenceDomain = await this.preferenceRepository.getByPreferenceId(
      id,
      true,
    );
    if (!preferenceDomain) {
      throw new NotFoundException(`Preference not found with id ${id}`);
    }
    const r = await this.preferenceRepository.restore(id);
    if (r) {
      preferenceDomain.deletedAt = null;
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.PREFERENCE,
        action: ACTIONS.RESTORE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return PreferenceResponse.fromDomain(preferenceDomain);
  }
  async deletePreference(id: string, currentUser: UserInfo): Promise<boolean> {
    const preferenceDomain = await this.preferenceRepository.getByPreferenceId(
      id,
      true,
    );
    if (!preferenceDomain) {
      throw new NotFoundException(`Preference not found with id ${id}`);
    }
    const result = await this.preferenceRepository.delete(id);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: id,
        modelName: MODELS.USER,
        action: ACTIONS.DELETE,
        userId: currentUser.id,
        user: currentUser,
      });
    }
    return result;
  }
}
