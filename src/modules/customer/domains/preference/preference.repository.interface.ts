import { Preference } from './preference';

export interface IPreferenceRepository {
  insert(preference: Preference): Promise<Preference>;
  update(preference: Preference): Promise<Preference>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Preference[]>;
  getById(id: string, withDeleted: boolean): Promise<Preference>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
