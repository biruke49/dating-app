export class Preference {
  id: string;
  customerId: string;
  minAge: number;
  maxAge: number;
  gender: string;
  religion: string;
  children: string;
  verified: boolean;
  location: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
