import { Customer } from "./customer";

export interface ICustomerRepository {
  insert(customer: Customer): Promise<Customer>;
  update(customer: Customer): Promise<Customer>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Customer[]>;
  getById(id: string, withDeleted: boolean): Promise<Customer>;
  getByPhoneNumber(phoneNumber: string, withDeleted: boolean): Promise<Customer>;
  getByEmail(email: string, withDeleted: boolean): Promise<Customer>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
