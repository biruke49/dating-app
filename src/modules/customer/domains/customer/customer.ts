import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { Profile } from '@customer/domains/profile/profile';

export class Customer {
  id: string;
  name: string;
  email: string;
  username: string;
  phoneNumber: string;
  dateOfBirth: Date;
  gender: string;
  enabled: boolean;
  profileImage: FileDto;
  address: Address;
  archiveReason: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  profile: Profile;
}
