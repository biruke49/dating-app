import { Profile } from "./profile";

export interface IProfileRepository {
  insert(profile: Profile): Promise<Profile>;
  update(profile: Profile): Promise<Profile>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Profile[]>;
  getById(id: string, withDeleted: boolean): Promise<Profile>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
