import { FileDto } from '@libs/common/file-dto';

export class Profile {
  id: string;
  // profileImage: FileDto;
  customerId: string;
  bio?: string;
  interests?: string[];
  education?: string;
  job?: string;
  height?: number;
  bodyType?: string;
  religion?: string;
  verified: boolean;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
  enabled: boolean;
  notifyMe: boolean;
}
