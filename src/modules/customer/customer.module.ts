import { Module } from '@nestjs/common';
import { CustomerController } from './controllers/customer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomerEntity } from './persistence/customer/customer.entity';
import { AccountModule } from '@account/account.module';
import { CustomerRepository } from './persistence/customer/customer.repository';
import { CustomerCommands } from './usecases/customer/customer.usecase.commands';
import { CustomerQuery } from './usecases/customer/customer.usecase.queries';
import { FileManagerService } from '@libs/common/file-manager';
import { ProfileEntity } from './persistence/profile/profile.entity';
import { ProfileController } from './controllers/profile.controller';
import { ProfileCommands } from './usecases/profile/profile.usecase.commands';
import { ProfileQuery } from './usecases/profile/profile.usecase.queries';
import { ProfileRepository } from './persistence/profile/profile.repository';
import { PreferenceController } from './controllers/preference.controller';
import { PreferenceEntity } from './persistence/preference/preference.entity';
import { PreferenceCommands } from './usecases/preference/preference.usecase.commands';
import { PreferenceQuery } from './usecases/preference/preference.usecase.queries';
import { PreferenceRepository } from './persistence/preference/preference.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([CustomerEntity, ProfileEntity, PreferenceEntity]),
    AccountModule,
  ],
  providers: [
    CustomerRepository,
    CustomerCommands,
    CustomerQuery,
    ProfileCommands,
    ProfileQuery,
    ProfileRepository,
    PreferenceCommands,
    PreferenceQuery,
    PreferenceRepository,
    FileManagerService,
  ],
  controllers: [CustomerController, ProfileController, PreferenceController],
})
export class CustomerModule {}
