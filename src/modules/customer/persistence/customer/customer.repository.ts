import { ICustomerRepository } from '../../domains/customer/customer.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomerEntity } from './customer.entity';
import { Customer } from '../../domains/customer/customer';
import { ProfileEntity } from '../profile/profile.entity';
import { Profile } from '@customer/domains/profile/profile';
@Injectable()
export class CustomerRepository implements ICustomerRepository {
  constructor(
    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>,
  ) {}
  async insert(customer: Customer): Promise<Customer> {
    const customerEntity = this.toCustomerEntity(customer);
    const result = await this.customerRepository.save(customerEntity);
    return result ? this.toCustomer(result) : null;
  }
  async update(customer: Customer): Promise<Customer> {
    const customerEntity = this.toCustomerEntity(customer);
    const result = await this.customerRepository.save(customerEntity);
    return result ? this.toCustomer(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.customerRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Customer[]> {
    const customers = await this.customerRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!customers.length) {
      return null;
    }
    return customers.map((customer) => this.toCustomer(customer));
  }
  async getById(id: string, withDeleted = false): Promise<Customer> {
    const customer = await this.customerRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!customer[0]) {
      return null;
    }
    return this.toCustomer(customer[0]);
  }
  async getByPhoneNumber(
    phoneNumber: string,
    withDeleted = false,
  ): Promise<Customer> {
    const customer = await this.customerRepository.find({
      where: { phoneNumber: phoneNumber },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!customer[0]) {
      return null;
    }
    return this.toCustomer(customer[0]);
  }
  async getByEmail(email: string, withDeleted = false): Promise<Customer> {
    const customer = await this.customerRepository.find({
      where: { email: email },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!customer[0]) {
      return null;
    }
    return this.toCustomer(customer[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.customerRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.customerRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toCustomer(customerEntity: CustomerEntity): Customer {
    const customer = new Customer();
    customer.id = customerEntity.id;
    customer.name = customerEntity.name;
    customer.email = customerEntity.email;
    customer.username = customerEntity.username;
    customer.dateOfBirth = customerEntity.dateOfBirth;
    customer.phoneNumber = customerEntity.phoneNumber;
    customer.gender = customerEntity.gender;
    customer.enabled = customerEntity.enabled;
    customer.profileImage = customerEntity.profileImage;
    customer.address = customerEntity.address;
    customer.archiveReason = customerEntity.archiveReason;
    customer.createdBy = customerEntity.createdBy;
    customer.updatedBy = customerEntity.updatedBy;
    customer.deletedBy = customerEntity.deletedBy;
    customer.createdAt = customerEntity.createdAt;
    customer.updatedAt = customerEntity.updatedAt;
    customer.deletedAt = customerEntity.deletedAt;
    return customer;
  }
  toCustomerEntity(customer: Customer): CustomerEntity {
    const customerEntity = new CustomerEntity();
    customerEntity.id = customer.id;
    customerEntity.name = customer.name;
    customerEntity.email = customer.email;
    customerEntity.username = customer.username;
    customerEntity.dateOfBirth = customer.dateOfBirth;
    customerEntity.phoneNumber = customer.phoneNumber;
    customerEntity.gender = customer.gender;
    customerEntity.enabled = customer.enabled;
    customerEntity.profileImage = customer.profileImage;
    customerEntity.address = customer.address;
    customerEntity.archiveReason = customer.archiveReason;
    customerEntity.createdBy = customer.createdBy;
    customerEntity.updatedBy = customer.updatedBy;
    customerEntity.deletedBy = customer.deletedBy;
    customerEntity.createdAt = customer.createdAt;
    customerEntity.updatedAt = customer.updatedAt;
    customerEntity.deletedAt = customer.deletedAt;
    return customerEntity;
  }
    toProfile(profileEntity: ProfileEntity): Profile {
    const profile = new Profile();
    profile.id = profileEntity.id;
    profile.customerId = profileEntity.customerId;
    profile.bio = profileEntity.bio;
    profile.interests = profileEntity.interests;
    profile.education = profileEntity.education;
    profile.job = profileEntity.job;
    profile.height = profileEntity.height;
    profile.bodyType = profileEntity.bodyType;
    // profile.profileImage = profileEntity.profileImage;
    profile.religion = profileEntity.religion;
    profile.verified = profileEntity.verified;
    profile.enabled = profileEntity.enabled;
    profile.notifyMe = profileEntity.notifyMe;
    profile.createdBy = profileEntity.createdBy;
    profile.updatedBy = profileEntity.updatedBy;
    profile.deletedBy = profileEntity.deletedBy;
    profile.createdAt = profileEntity.createdAt;
    profile.updatedAt = profileEntity.updatedAt;
    profile.deletedAt = profileEntity.deletedAt;
    return profile;
  }
  toProfileEntity(profile: Profile): ProfileEntity {
    const profileEntity = new ProfileEntity();
    profileEntity.id = profile.id;
    profileEntity.customerId = profile.customerId;
    profileEntity.bio = profile.bio;
    profileEntity.interests = profile.interests;
    profileEntity.education = profile.education;
    profileEntity.job = profile.job;
    profileEntity.height = profile.height;
    profileEntity.bodyType = profile.bodyType;
    // profileEntity.profileImage = profile.profileImage;
    profileEntity.religion = profile.religion;
    profileEntity.verified = profile.verified;
    profileEntity.enabled = profile.enabled;
    profileEntity.notifyMe = profile.notifyMe;
    profileEntity.createdBy = profile.createdBy;
    profileEntity.updatedBy = profile.updatedBy;
    profileEntity.deletedBy = profile.deletedBy;
    profileEntity.createdAt = profile.createdAt;
    profileEntity.updatedAt = profile.updatedAt;
    profileEntity.deletedAt = profile.deletedAt;
    return profileEntity;
  }
}
