// src/users/user.entity.ts
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
} from 'typeorm';
import { Address } from '@libs/common/address';
import { FileDto } from '@libs/common/file-dto';
import { ProfileEntity } from '@customer/persistence/profile/profile.entity';
import { CommonEntity } from '@libs/common/common.entity';
import { PreferenceEntity } from '../preference/preference.entity';

@Entity('customers')
export class CustomerEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  name: string;
  @Column({ unique: true })
  email: string;
  @Column({ length: 30 })
  username: string;
  @Column({ name: 'phone_number', unique: true })
  phoneNumber: string;
  @Column({ type: 'date' })
  dateOfBirth: Date;
  @Column()
  gender: string;
  @Column({ type: 'jsonb', nullable: true })
  address: Address;
  @Column({ name: 'enabled', default: true })
  enabled: boolean;
  @Column({ name: 'profile_image', type: 'jsonb', nullable: true })
  profileImage: FileDto;
  //   @OneToMany(() => Match, (match) => match.user1, { cascade: true })
  //   matchesAsUser1: Match[];
  //   @OneToMany(() => Match, (match) => match.user2, { cascade: true })
  //   matchesAsUser2: Match[];
  @OneToOne(() => ProfileEntity, (profile) => profile.customer)
  profile: ProfileEntity;
  @OneToOne(() => PreferenceEntity, (preference) => preference.customer)
  preference: PreferenceEntity;
}
