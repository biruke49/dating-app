import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProfileEntity } from './profile.entity';
import { Profile } from '@customer/domains/profile/profile';
import { IProfileRepository } from '@customer/domains/profile/profile.repository.interface';
@Injectable()
export class ProfileRepository implements IProfileRepository {
  constructor(
    @InjectRepository(ProfileEntity)
    private profileRepository: Repository<ProfileEntity>,
  ) {}
  async insert(profile: Profile): Promise<Profile> {
    const profileEntity = this.toProfileEntity(profile);
    const result = await this.profileRepository.save(profileEntity);
    return result ? this.toProfile(result) : null;
  }
  async update(profile: Profile): Promise<Profile> {
    const profileEntity = this.toProfileEntity(profile);
    const result = await this.profileRepository.save(profileEntity);
    return result ? this.toProfile(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.profileRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Profile[]> {
    const profiles = await this.profileRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!profiles.length) {
      return null;
    }
    return profiles.map((profile) => this.toProfile(profile));
  }
  async getById(id: string, withDeleted = false): Promise<Profile> {
    const profile = await this.profileRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!profile[0]) {
      return null;
    }
    return this.toProfile(profile[0]);
  }
  async getByCustomerId(customerId: string, withDeleted = false): Promise<Profile> {
    const profile = await this.profileRepository.find({
      where: { customerId: customerId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!profile[0]) {
      return null;
    }
    return this.toProfile(profile[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.profileRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.profileRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toProfile(profileEntity: ProfileEntity): Profile {
    const profile = new Profile();
    profile.id = profileEntity.id;
    profile.customerId = profileEntity.customerId;
    profile.bio = profileEntity.bio;
    profile.interests = profileEntity.interests;
    profile.education = profileEntity.education;
    profile.job = profileEntity.job;
    profile.height = profileEntity.height;
    profile.bodyType = profileEntity.bodyType;
    profile.religion = profileEntity.religion;
    profile.verified = profileEntity.verified;
    profile.enabled = profileEntity.enabled;
    profile.notifyMe = profileEntity.notifyMe;
    profile.createdBy = profileEntity.createdBy;
    profile.updatedBy = profileEntity.updatedBy;
    profile.deletedBy = profileEntity.deletedBy;
    profile.createdAt = profileEntity.createdAt;
    profile.updatedAt = profileEntity.updatedAt;
    profile.deletedAt = profileEntity.deletedAt;
    return profile;
  }
  toProfileEntity(profile: Profile): ProfileEntity {
    const profileEntity = new ProfileEntity();
    profileEntity.id = profile.id;
    profileEntity.customerId = profile.customerId;
    profileEntity.bio = profile.bio;
    profileEntity.interests = profile.interests;
    profileEntity.education = profile.education;
    profileEntity.job = profile.job;
    profileEntity.height = profile.height;
    profileEntity.bodyType = profile.bodyType;
    profileEntity.religion = profile.religion;
    profileEntity.verified = profile.verified;
    profileEntity.enabled = profile.enabled;
    profileEntity.notifyMe = profile.notifyMe;
    profileEntity.createdBy = profile.createdBy;
    profileEntity.updatedBy = profile.updatedBy;
    profileEntity.deletedBy = profile.deletedBy;
    profileEntity.createdAt = profile.createdAt;
    profileEntity.updatedAt = profile.updatedAt;
    profileEntity.deletedAt = profile.deletedAt;
    return profileEntity;
  }
}
