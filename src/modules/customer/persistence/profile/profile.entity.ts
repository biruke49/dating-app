import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { CommonEntity } from '@libs/common/common.entity';
import { CustomerEntity } from '@customer/persistence/customer/customer.entity';

@Entity('profiles')
export class ProfileEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'customer_id', type: 'uuid' })
  customerId: string;
  @Column({ nullable: true })
  bio: string;
  @Column('simple-array', { nullable: true })
  interests: string[];
  @Column({ nullable: true })
  education: string;
  @Column({ nullable: true })
  job: string;
  @Column({ nullable: true })
  height: number;
  @Column({ name: 'body_type', nullable: true })
  bodyType: string;
  @Column({ nullable: true })
  religion: string;
  @Column({ default: false })
  verified: boolean;
  @Column({ default: true })
  enabled: boolean;
  @Column({ default: true })
  notifyMe: boolean;
  @OneToOne(() => CustomerEntity, (customer) => customer.profile, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'customer_id' })
  customer: CustomerEntity;
}
