import { IPreferenceRepository } from '@customer/domains/preference/preference.repository.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PreferenceEntity } from './preference.entity';
import { Preference } from '@customer/domains/preference/preference';
@Injectable()
export class PreferenceRepository implements IPreferenceRepository {
  constructor(
    @InjectRepository(PreferenceEntity)
    private preferenceRepository: Repository<PreferenceEntity>,
  ) {}
  async insert(preference: Preference): Promise<Preference> {
    const preferenceEntity = this.toPreferenceEntity(preference);
    const result = await this.preferenceRepository.save(preferenceEntity);
    return result ? this.toPreference(result) : null;
  }
  async update(preference: Preference): Promise<Preference> {
    const preferenceEntity = this.toPreferenceEntity(preference);
    const result = await this.preferenceRepository.save(preferenceEntity);
    return result ? this.toPreference(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Preference[]> {
    const preferences = await this.preferenceRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preferences.length) {
      return null;
    }
    return preferences.map((preference) => this.toPreference(preference));
  }
  async getById(id: string, withDeleted = false): Promise<Preference> {
    const preference = await this.preferenceRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toPreference(preference[0]);
  }
  async getByPreferenceId(
    customerId: string,
    withDeleted = false,
  ): Promise<Preference> {
    const preference = await this.preferenceRepository.find({
      where: { customerId: customerId },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!preference[0]) {
      return null;
    }
    return this.toPreference(preference[0]);
  }
  async archive(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.preferenceRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toPreference(preferenceEntity: PreferenceEntity): Preference {
    const preference = new Preference();
    preference.id = preferenceEntity.id;
    preference.customerId = preferenceEntity.customerId;
    preference.minAge = preferenceEntity.minAge;
    preference.maxAge = preferenceEntity.maxAge;
    preference.gender = preferenceEntity.gender;
    preference.children = preferenceEntity.children;
    preference.location = preferenceEntity.location;
    preference.religion = preferenceEntity.religion;
    preference.verified = preferenceEntity.verified;
    preference.createdBy = preferenceEntity.createdBy;
    preference.updatedBy = preferenceEntity.updatedBy;
    preference.deletedBy = preferenceEntity.deletedBy;
    preference.createdAt = preferenceEntity.createdAt;
    preference.updatedAt = preferenceEntity.updatedAt;
    preference.deletedAt = preferenceEntity.deletedAt;
    return preference;
  }
  toPreferenceEntity(preference: Preference): PreferenceEntity {
    const preferenceEntity = new PreferenceEntity();
    preferenceEntity.id = preference.id;
    preferenceEntity.customerId = preference.customerId;
    preferenceEntity.minAge = preference.minAge;
    preferenceEntity.maxAge = preference.maxAge;
    preferenceEntity.gender = preference.gender;
    preferenceEntity.children = preference.children;
    preferenceEntity.location = preference.location;
    preferenceEntity.religion = preference.religion;
    preferenceEntity.verified = preference.verified;
    preferenceEntity.createdBy = preference.createdBy;
    preferenceEntity.updatedBy = preference.updatedBy;
    preferenceEntity.deletedBy = preference.deletedBy;
    preferenceEntity.createdAt = preference.createdAt;
    preferenceEntity.updatedAt = preference.updatedAt;
    preferenceEntity.deletedAt = preference.deletedAt;
    return preferenceEntity;
  }
}
