import { Customer } from '@customer/domains/customer/customer';
import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CustomerEntity } from '../customer/customer.entity';

@Entity('preferences')
export class PreferenceEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ name: 'customer_id', type: 'uuid' })
  customerId: string;
  @Column({ default: 18 })
  minAge: number;
  @Column({ default: 99 })
  maxAge: number;
  @Column()
  gender: string;
  @Column()
  religion: string;
  @Column()
  children: string;
  @Column()
  verified: boolean;
  @Column()
  location: string;
  @OneToOne(() => CustomerEntity, (customer) => customer.preference, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'customer_id' })
  customer: CustomerEntity;
}
