import { GlobalConfigurations } from '@configurations/usecases/configuration/configuration.commands';

export class Configuration {
  id: string;
  globalConfigurations: GlobalConfigurations;
  archiveReason?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  deletedBy?: string;
}
