import { Service } from './service';

export interface IServiceRepository {
  insert(service: Service): Promise<Service>;
  update(user: Service): Promise<Service>;
  delete(id: string): Promise<boolean>;
  getServices(withDeleted: boolean): Promise<Service[]>;
  getServiceById(id: string, withDeleted: boolean): Promise<Service>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
