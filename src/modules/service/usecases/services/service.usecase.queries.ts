import { CollectionQuery } from '@libs/collection-query/collection-query';
import { FilterOperators } from '@libs/collection-query/filter_operators';
import { QueryConstructor } from '@libs/collection-query/query-constructor';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ServiceEntity } from '@service/persistence/services/service.entity';
import { Repository } from 'typeorm';
import { ServiceResponse } from './service.response';

@Injectable()
export class ServiceQuery {
  constructor(
    @InjectRepository(ServiceEntity)
    private serviceRepository: Repository<ServiceEntity>,
  ) {}
  async getServices(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ServiceResponse>> {
    const dataQuery = QueryConstructor.constructQuery<ServiceEntity>(
      this.serviceRepository,
      query,
    );
    const d = new DataResponseFormat<ServiceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ServiceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
  async getServiceById(
    id: string,
    withDeleted = false,
  ): Promise<ServiceResponse> {
    const service = await this.serviceRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!service[0]) {
      throw new NotFoundException(`Service not found with id ${id}`);
    }
    return ServiceResponse.fromEntity(service[0]);
  }
  async getArchivedServiceById(
    id: string,
    withDeleted = false,
  ): Promise<ServiceResponse> {
    const service = await this.serviceRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!service[0]) {
      throw new NotFoundException(`Service not found with id ${id}`);
    }
    return ServiceResponse.fromEntity(service[0]);
  }
  async getArchivedServices(
    query: CollectionQuery,
  ): Promise<DataResponseFormat<ServiceResponse>> {
    if (!query.filter) {
      query.filter = [];
    }
    query.filter.push([
      {
        field: 'deleted_at',
        operator: FilterOperators.NotNull,
      },
    ]);
    const dataQuery = QueryConstructor.constructQuery<ServiceEntity>(
      this.serviceRepository,
      query,
    );
    dataQuery.withDeleted();
    const d = new DataResponseFormat<ServiceResponse>();
    if (query.count) {
      d.count = await dataQuery.getCount();
    } else {
      const [result, total] = await dataQuery.getManyAndCount();
      d.data = result.map((entity) => ServiceResponse.fromEntity(entity));
      d.count = total;
    }
    return d;
  }
}
