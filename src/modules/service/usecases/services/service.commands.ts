import { UserInfo } from '@account/dtos/user-info.dto';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { Service } from '@service/domains/services/service';
import { IsNotEmpty } from 'class-validator';

export class CreateServiceCommand {
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  description: string;
  coverImage: FileDto;
  currentUser: UserInfo;
  static fromCommand(command: CreateServiceCommand): Service {
    const serviceDomain = new Service();
    serviceDomain.title = command.title;
    serviceDomain.description = command.description;
    serviceDomain.coverImage = command.coverImage;
    return serviceDomain;
  }
}
export class UpdateServiceCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  description: string;
  coverImage: FileDto;
  currentUser: UserInfo;
}
export class ArchiveServiceCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
