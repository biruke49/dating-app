import { diskStorage } from 'multer';

import { AllowAnonymous } from '@account/decorators/allow-anonymous.decorator';
import { CurrentUser } from '@account/decorators/current-user.decorator';
import { UserInfo } from '@account/dtos/user-info.dto';
import { PermissionsGuard } from '@account/guards/permission.quard';
import { CollectionQuery } from '@libs/collection-query/collection-query';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { ApiPaginatedResponse } from '@libs/response-format/api-paginated-response';
import { DataResponseFormat } from '@libs/response-format/data-response-format';
import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiConsumes,
  ApiExtraModels,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ArchiveServiceCommand,
  CreateServiceCommand,
  UpdateServiceCommand,
} from '@service/usecases/services/service.commands';
import { ServiceResponse } from '@service/usecases/services/service.response';
import { ServiceCommands } from '@service/usecases/services/service.usecase.commands';
import { ServiceQuery } from '@service/usecases/services/service.usecase.queries';

@Controller('services')
@ApiTags('services')
@ApiResponse({ status: 500, description: 'Internal error' })
@ApiResponse({ status: 404, description: 'Item not found' })
@ApiExtraModels(DataResponseFormat)
export class ServicesController {
  constructor(
    private queries: ServiceQuery,
    private command: ServiceCommands,
    private readonly fileManagerService: FileManagerService,
  ) {}
  @Get('get-service/:id')
  @ApiOkResponse({ type: ServiceResponse })
  async getServiceById(@Param('id') id: string) {
    return this.queries.getServiceById(id);
  }
  @Get('get-archived-service/:id')
  @ApiOkResponse({ type: ServiceResponse })
  async getArchivedServiceById(@Param('id') id: string) {
    return this.queries.getArchivedServiceById(id, true);
  }
  @Get('get-services')
  @AllowAnonymous()
  @ApiPaginatedResponse(ServiceResponse)
  async getServices(@Query() query: CollectionQuery) {
    return this.queries.getServices(query);
  }
  @Post('create-service')
  @UseGuards(PermissionsGuard('manage-service'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('coverImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (!file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: ServiceResponse })
  async createService(
    @CurrentUser() user: UserInfo,
    @Body() createServiceCommand: CreateServiceCommand,
    @UploadedFile() coverImage: Express.Multer.File,
  ) {
    if (coverImage) {
      const result = await this.fileManagerService.uploadFile(
        coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        createServiceCommand.coverImage = result;
      }
    }
    createServiceCommand.currentUser = user;
    return this.command.createService(createServiceCommand);
  }
  @Put('update-service')
  @UseGuards(PermissionsGuard('manage-service'))
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(
    FileInterceptor('coverImage', {
      storage: diskStorage({
        destination: FileManagerHelper.UPLOADED_FILES_DESTINATION,
      }),
      fileFilter: (request, file, callback) => {
        if (file && !file.mimetype.includes('image')) {
          return callback(
            new BadRequestException('Provide a valid image'),
            false,
          );
        }
        callback(null, true);
      },
      limits: { fileSize: Math.pow(1024, 2) },
    }),
  )
  @ApiOkResponse({ type: ServiceResponse })
  async updateService(
    @CurrentUser() user: UserInfo,
    @Body() updateServiceCommand: UpdateServiceCommand,
    @UploadedFile() coverImage: Express.Multer.File,
  ) {
    if (coverImage) {
      const result = await this.fileManagerService.uploadFile(
        coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
      if (result) {
        updateServiceCommand.coverImage = result;
      }
    }
    updateServiceCommand.currentUser = user;
    return this.command.updateService(updateServiceCommand);
  }
  @Delete('delete-service/:id')
  @UseGuards(PermissionsGuard('manage-service'))
  @ApiOkResponse({ type: Boolean })
  async deleteService(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.deleteService(id, user);
  }
  @Delete('archive-service')
  @UseGuards(PermissionsGuard('manage-service'))
  @ApiOkResponse({ type: ServiceResponse })
  async archiveService(
    @CurrentUser() user: UserInfo,
    @Body() command: ArchiveServiceCommand,
  ) {
    command.currentUser = user;
    return this.command.archiveService(command);
  }
  @Get('get-archived-services')
  @ApiPaginatedResponse(ServiceResponse)
  async getArchivedServices(@Query() query: CollectionQuery) {
    return this.queries.getArchivedServices(query);
  }
  @Post('restore-service/:id')
  @UseGuards(PermissionsGuard('manage-service'))
  @ApiOkResponse({ type: ServiceResponse })
  async restoreService(@CurrentUser() user: UserInfo, @Param('id') id: string) {
    return this.command.restoreService(id, user);
  }
}
