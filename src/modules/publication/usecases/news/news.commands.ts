import { UserInfo } from '@account/dtos/user-info.dto';
import { FileDto } from '@libs/common/file-dto';
import { ApiProperty } from '@nestjs/swagger';
import { News } from '@publication/domains/news';
import { IsNotEmpty } from 'class-validator';

export class CreateNewsCommand {
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  description: string;
  coverImage: FileDto;
  currentUser: UserInfo;
  static fromCommand(command: CreateNewsCommand): News {
    const newsDomain = new News();
    newsDomain.title = command.title;
    newsDomain.description = command.description;
    newsDomain.coverImage = command.coverImage;
    return newsDomain;
  }
}
export class UpdateNewsCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  description: string;
  coverImage: FileDto;
  currentUser: UserInfo;
}
export class ArchiveNewsCommand {
  @ApiProperty({
    example: 'd02dd06f-2a30-4ed8-a2a0-75c683e3092e',
  })
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
