import { ACTIONS, MODELS } from '@activity-logger/domains/activities/constants';
import { UserInfo } from '@account/dtos/user-info.dto';
import {
  FileManagerHelper,
  FileManagerService,
} from '@libs/common/file-manager';
import { Injectable, NotFoundException } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { NewsRepository } from '@publication/persistence/news/news.repository';
import {
  ArchiveNewsCommand,
  CreateNewsCommand,
  UpdateNewsCommand,
} from './news.commands';
import { NewsResponse } from './news.response';

@Injectable()
export class NewsCommands {
  constructor(
    private newsRepository: NewsRepository,
    private readonly eventEmitter: EventEmitter2,
    private readonly fileManagerService: FileManagerService,
  ) {}
  async createNews(command: CreateNewsCommand): Promise<NewsResponse> {
    const newsDomain = CreateNewsCommand.fromCommand(command);
    const news = await this.newsRepository.insert(newsDomain);
    if (news) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: news.id,
        modelName: MODELS.NEWS,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
    }
    return NewsResponse.fromDomain(news);
  }
  async updateNews(command: UpdateNewsCommand): Promise<NewsResponse> {
    const newsDomain = await this.newsRepository.getNewsById(command.id);
    if (!newsDomain) {
      throw new NotFoundException(`News not found with id ${command.id}`);
    }
    if (newsDomain.coverImage && command.coverImage) {
      await this.fileManagerService.removeFile(
        newsDomain.coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    const oldPayload = { ...newsDomain };
    newsDomain.title = command.title;
    newsDomain.description = command.description;
    if (
      command.coverImage &&
      command.coverImage?.filename !== newsDomain.coverImage?.filename
    ) {
      newsDomain.coverImage = command.coverImage;
    }
    const news = await this.newsRepository.update(newsDomain);
    if (news) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: news.id,
        modelName: MODELS.NEWS,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: { ...news },
        oldPayload: { ...oldPayload },
      });
    }
    return NewsResponse.fromDomain(news);
  }
  async deleteNews(id: string, currentUser: UserInfo): Promise<boolean> {
    const newsDomain = await this.newsRepository.getNewsById(id, true);
    if (!newsDomain) {
      throw new NotFoundException(`News not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.NEWS,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    if (newsDomain.coverImage) {
      await this.fileManagerService.removeFile(
        newsDomain.coverImage,
        FileManagerHelper.UPLOADED_FILES_DESTINATION,
      );
    }
    return await this.newsRepository.delete(id);
  }
  async archiveNews(command: ArchiveNewsCommand): Promise<NewsResponse> {
    const newsDomain = await this.newsRepository.getNewsById(command.id);
    if (!newsDomain) {
      throw new NotFoundException(`News not found with id ${command.id}`);
    }
    newsDomain.deletedAt = new Date();
    newsDomain.deletedBy = command.currentUser.id;
    newsDomain.archiveReason = command.reason;
    const result = await this.newsRepository.update(newsDomain);
    if (result) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: command.id,
        modelName: MODELS.NEWS,
        action: ACTIONS.ARCHIVE,
        userId: command.currentUser.id,
        user: command.currentUser,
      });
    }
    return NewsResponse.fromDomain(result);
  }
  async restoreNews(id: string, currentUser: UserInfo): Promise<NewsResponse> {
    const newsDomain = await this.newsRepository.getNewsById(id, true);
    if (!newsDomain) {
      throw new NotFoundException(`Category not found with id ${id}`);
    }
    const r = await this.newsRepository.restore(id);
    if (r) {
      newsDomain.deletedAt = null;
      newsDomain.deletedBy = null;
      newsDomain.archiveReason = null;
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.NEWS,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    return NewsResponse.fromDomain(newsDomain);
  }
}
