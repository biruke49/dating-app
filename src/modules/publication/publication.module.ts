import { FileManagerService } from '@libs/common/file-manager';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NewsController } from './contollers/news.controller';
import { NewsEntity } from './persistence/news/news.entity';
import { NewsRepository } from './persistence/news/news.repository';
import { NewsCommands } from './usecases/news/news.usecase.commands';
import { NewsQuery } from './usecases/news/news.usecase.queries';
@Module({
  controllers: [NewsController],
  imports: [TypeOrmModule.forFeature([NewsEntity])],
  providers: [NewsCommands, NewsQuery, NewsRepository, FileManagerService],
})
export class PublicationModule {}
