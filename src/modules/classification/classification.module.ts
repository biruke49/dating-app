import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryCommands } from './usecases/categories/category.usecase.commands';
import { CategoriesController } from './controllers/category.controller';
import { Module } from '@nestjs/common';
import { CategoryQuery } from './usecases/categories/category.usecase.queries';
import { CategoryRepository } from './persistence/categories/category.repository';
import { CategoryEntity } from './persistence/categories/category.entity';
import { ConfigurationRepository } from '@configurations/persistence/configuration/configuration.repository';
import { ConfigurationEntity } from '@configurations/persistence/configuration/configuration.entity';
import { AccountRepository } from '@account/persistence/accounts/account.repository';
import { AccountEntity } from '@account/persistence/accounts/account.entity';
@Module({
  controllers: [CategoriesController],
  imports: [
    TypeOrmModule.forFeature([
      CategoryEntity,
      ConfigurationEntity,
      AccountEntity,
    ]),
  ],
  providers: [
    CategoryCommands,
    CategoryQuery,
    CategoryRepository,
    ConfigurationRepository,
    AccountRepository,
  ],
})
export class ClassificationModule {}
