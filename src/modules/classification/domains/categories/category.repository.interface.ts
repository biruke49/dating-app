import { Category } from './category';
export interface ICategoryRepository {
  insert(user: Category): Promise<Category>;
  update(user: Category): Promise<Category>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Category[]>;
  getById(id: string, withDeleted: boolean): Promise<Category>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
