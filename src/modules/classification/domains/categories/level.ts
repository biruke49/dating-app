import { ApiProperty } from '@nestjs/swagger';

export class Level {
  @ApiProperty()
  easy: number;
  @ApiProperty()
  moderate: number;
  @ApiProperty()
  difficult: number;
  @ApiProperty()
  extreme: number;
}
