import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from '@classification/domains/categories/category';
import { ICategoryRepository } from '@classification/domains/categories/category.repository.interface';
import { Repository } from 'typeorm';
@Injectable()
export class CategoryRepository implements ICategoryRepository {
  constructor(
    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}
  async insert(category: Category): Promise<Category> {
    const categoryEntity = this.toCategoryEntity(category);
    const result = await this.categoryRepository.save(categoryEntity);
    return result ? this.toCategory(result) : null;
  }
  async update(category: Category): Promise<Category> {
    const categoryEntity = this.toCategoryEntity(category);
    const result = await this.categoryRepository.save(categoryEntity);
    return result ? this.toCategory(result) : null;
  }
  async delete(id: string): Promise<boolean> {
    const result = await this.categoryRepository.delete({ id: id });
    if (result.affected > 0) return true;
    return false;
  }
  async getAll(withDeleted: boolean): Promise<Category[]> {
    const categories = await this.categoryRepository.find({
      relations: [],
      withDeleted: withDeleted,
    });
    if (!categories.length) {
      return null;
    }
    return categories.map((category) => this.toCategory(category));
  }
  async getById(id: string, withDeleted = false): Promise<Category> {
    const category = await this.categoryRepository.find({
      where: { id: id },
      relations: [],
      withDeleted: withDeleted,
    });
    if (!category[0]) {
      return null;
    }
    return this.toCategory(category[0]);
  }

  async archive(id: string): Promise<boolean> {
    const result = await this.categoryRepository.softDelete(id);
    if (result.affected > 0) return true;
    return false;
  }
  async restore(id: string): Promise<boolean> {
    const result = await this.categoryRepository.restore(id);
    if (result.affected > 0) return true;
    return false;
  }
  toCategory(categoryEntity: CategoryEntity): Category {
    const category = new Category();
    category.id = categoryEntity.id;
    category.name = categoryEntity.name;
    category.description = categoryEntity.description;
    category.capacity = categoryEntity.capacity;
    category.archiveReason = categoryEntity.archiveReason;
    category.createdBy = categoryEntity.createdBy;
    category.updatedBy = categoryEntity.updatedBy;
    category.deletedBy = categoryEntity.deletedBy;
    category.createdAt = categoryEntity.createdAt;
    category.updatedAt = categoryEntity.updatedAt;
    category.deletedAt = categoryEntity.deletedAt;
    return category;
  }
  toCategoryEntity(category: Category): CategoryEntity {
    const categoryEntity = new CategoryEntity();
    categoryEntity.id = category.id;
    categoryEntity.name = category.name;
    categoryEntity.description = category.description;
    categoryEntity.capacity = category.capacity;
    categoryEntity.archiveReason = category.archiveReason;
    categoryEntity.createdBy = category.createdBy;
    categoryEntity.updatedBy = category.updatedBy;
    categoryEntity.deletedBy = category.deletedBy;
    categoryEntity.createdAt = category.createdAt;
    categoryEntity.updatedAt = category.updatedAt;
    categoryEntity.deletedAt = category.deletedAt;
    return categoryEntity;
  }
}
