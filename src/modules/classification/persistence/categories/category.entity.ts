import { Level } from '@classification/domains/categories/level';
import { CommonEntity } from '@libs/common/common.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('categories')
export class CategoryEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ unique: true })
  name: string;
  @Column({ type: 'integer' })
  capacity: number;
  @Column({ nullable: true })
  description: string;

}
