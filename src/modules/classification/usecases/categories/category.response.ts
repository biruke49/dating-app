import { Category } from '@classification/domains/categories/category';
import { ApiProperty } from '@nestjs/swagger';
import { CategoryEntity } from '@classification/persistence/categories/category.entity';
import { Level } from '@classification/domains/categories/level';

export class CategoryResponse {
  @ApiProperty()
  id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  capacity: number;
  @ApiProperty()
  description: string;
  @ApiProperty()
  archiveReason: string;
  @ApiProperty()
  createdBy?: string;
  @ApiProperty()
  updatedBy?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  deletedBy?: string;
  static fromEntity(categoryEntity: CategoryEntity): CategoryResponse {
    const categoryResponse = new CategoryResponse();
    categoryResponse.id = categoryEntity.id;
    categoryResponse.name = categoryEntity.name;
    categoryResponse.description = categoryEntity.description;
    categoryResponse.capacity = categoryEntity.capacity;
    categoryResponse.archiveReason = categoryEntity.archiveReason;
    categoryResponse.createdBy = categoryEntity.createdBy;
    categoryResponse.updatedBy = categoryEntity.updatedBy;
    categoryResponse.deletedBy = categoryEntity.deletedBy;
    categoryResponse.createdAt = categoryEntity.createdAt;
    categoryResponse.updatedAt = categoryEntity.updatedAt;
    categoryResponse.deletedAt = categoryEntity.deletedAt;
    return categoryResponse;
  }
  static fromDomain(category: Category): CategoryResponse {
    const categoryResponse = new CategoryResponse();
    categoryResponse.id = category.id;
    categoryResponse.name = category.name;
    categoryResponse.description = category.description;
    categoryResponse.capacity = category.capacity;
    categoryResponse.archiveReason = category.archiveReason;
    categoryResponse.createdBy = category.createdBy;
    categoryResponse.updatedBy = category.updatedBy;
    categoryResponse.deletedBy = category.deletedBy;
    categoryResponse.createdAt = category.createdAt;
    categoryResponse.updatedAt = category.updatedAt;
    categoryResponse.deletedAt = category.deletedAt;
    return categoryResponse;
  }
}
