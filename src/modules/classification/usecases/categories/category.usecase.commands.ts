import { MODELS, ACTIONS } from '@activity-logger/domains/activities/constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import {
  ArchiveCategoryCommand,
  CreateCategoryCommand,
  UpdateCategoryCommand,
} from './category.commands';
import { CategoryRepository } from '@classification/persistence/categories/category.repository';
import { CategoryResponse } from './category.response';
import { Injectable, NotFoundException } from '@nestjs/common';
import { UserInfo } from '@account/dtos/user-info.dto';
import { CollectionQuery } from '@libs/collection-query/collection-query';
@Injectable()
export class CategoryCommands {
  constructor(
    private categoryRepository: CategoryRepository,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  async createCategory(
    command: CreateCategoryCommand,
  ): Promise<CategoryResponse> {
    const categoryDomain = CreateCategoryCommand.fromCommand(command);
    categoryDomain.createdBy = command.currentUser.id;
    categoryDomain.updatedBy = command.currentUser.id;
    const category = await this.categoryRepository.insert(categoryDomain);
    if (category) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: category.id,
        modelName: MODELS.CATEGORY,
        action: ACTIONS.CREATE,
        user: command.currentUser,
        userId: command.currentUser.id,
      });
      this.eventEmitter.emit('add.new.route.price.to.routes', category);
    }
    return CategoryResponse.fromDomain(category);
  }
  async updateCategory(
    command: UpdateCategoryCommand,
  ): Promise<CategoryResponse> {
    const categoryDomain = await this.categoryRepository.getById(command.id);
    if (!categoryDomain) {
      throw new NotFoundException(`Category not found with id ${command.id}`);
    }

    const oldPayload = { ...categoryDomain };
    categoryDomain.name = command.name;
    categoryDomain.description = command.description;
    categoryDomain.capacity = command.capacity;
    categoryDomain.updatedBy = command.currentUser.id;
    const category = await this.categoryRepository.update(categoryDomain);
    if (category) {
      this.eventEmitter.emit('activity-logger.store', {
        modelId: category.id,
        modelName: MODELS.CATEGORY,
        action: ACTIONS.UPDATE,
        userId: command.currentUser.id,
        user: command.currentUser,
        payload: category,
        oldPayload: oldPayload,
      });
      this.eventEmitter.emit('update.route.price.to.routes', category);
    }
    return CategoryResponse.fromDomain(category);
  }
  async archiveCategory(
    command: ArchiveCategoryCommand,
  ): Promise<CategoryResponse> {
    const categoryDomain = await this.categoryRepository.getById(command.id);
    if (!categoryDomain) {
      throw new NotFoundException(`Category not found with id ${command.id}`);
    }

    this.eventEmitter.emit('archive.route.price.to.routes', categoryDomain);
    categoryDomain.deletedAt = new Date();
    categoryDomain.archiveReason = command.reason;
    categoryDomain.deletedBy = command.currentUser.id;
    const result = await this.categoryRepository.update(categoryDomain);

    this.eventEmitter.emit('activity-logger.store', {
      modelId: command.id,
      modelName: MODELS.CATEGORY,
      action: ACTIONS.ARCHIVE,
      userId: command.currentUser.id,
      user: command.currentUser,
    });
    return CategoryResponse.fromDomain(categoryDomain);
  }
  async restoreCategory(
    id: string,
    currentUser: UserInfo,
  ): Promise<CategoryResponse> {
    const categoryDomain = await this.categoryRepository.getById(id, true);
    if (!categoryDomain) {
      throw new NotFoundException(`Category not found with id ${id}`);
    }
    const r = await this.categoryRepository.restore(id);
    if (r) {
      categoryDomain.deletedAt = null;
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.CATEGORY,
      action: ACTIONS.RESTORE,
      userId: currentUser.id,
      user: currentUser,
    });
    this.eventEmitter.emit('restore.route.price.to.routes', categoryDomain);
    return CategoryResponse.fromDomain(categoryDomain);
  }
  async deleteCategory(id: string, currentUser: UserInfo): Promise<boolean> {
    const categoryDomain = await this.categoryRepository.getById(id, true);
    if (!categoryDomain) {
      throw new NotFoundException(`Category not found with id ${id}`);
    }
    this.eventEmitter.emit('activity-logger.store', {
      modelId: id,
      modelName: MODELS.CATEGORY,
      action: ACTIONS.DELETE,
      userId: currentUser.id,
      user: currentUser,
    });
    this.eventEmitter.emit('delete.route.price.to.routes', categoryDomain);
    return await this.categoryRepository.delete(id);
  }
}
