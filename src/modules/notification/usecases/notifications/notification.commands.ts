import { UserInfo } from '@account/dtos/user-info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Notification } from '@notification/domains/notifications/notification';
import { IsNotEmpty } from 'class-validator';

export class CreateNotificationCommand {
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  body: string;
  @ApiProperty()
  receiver: string;
  @ApiProperty()
  type: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  isSeen: boolean;
  static fromCommand(command: CreateNotificationCommand): Notification {
    const notification = new Notification();
    notification.title = command.title;
    notification.body = command.body;
    notification.receiver = command.receiver;
    notification.type = command.type;
    notification.status = command.status;
    notification.isSeen = command.isSeen;
    return notification;
  }
}

export class UpdateNotificationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @ApiProperty()
  @IsNotEmpty()
  body: string;
  @ApiProperty()
  @IsNotEmpty()
  receiver: string;
  @ApiProperty()
  @IsNotEmpty()
  type: string;
  @ApiProperty()
  @IsNotEmpty()
  status: string;
  @ApiProperty()
  isSeen: boolean;
  static fromCommand(command: UpdateNotificationCommand): Notification {
    const notification = new Notification();
    notification.id = command.id;
    notification.title = command.title;
    notification.body = command.body;
    notification.receiver = command.receiver;
    notification.type = command.type;
    notification.status = command.status;
    notification.isSeen = command.isSeen;
    return notification;
  }
}
export class ArchiveNotificationCommand {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  reason: string;
  currentUser: UserInfo;
}
export class SendSmsCommand {
  @ApiProperty()
  @IsNotEmpty()
  phone: string;
  @ApiProperty()
  @IsNotEmpty()
  message: string;
  receiver: string;
}