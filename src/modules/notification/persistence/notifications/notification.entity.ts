import { AccountEntity } from '@account/persistence/accounts/account.entity';
import { CommonEntity } from '@libs/common/common.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('notifications')
export class NotificationEntity extends CommonEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  title: string;
  @Column({ type: 'text' })
  body: string;
  @Column({ type: 'uuid', nullable: true })
  receiver: string;
  @Column({ nullable: true })
  type: string;
  @Column({ nullable: true })
  status: string;
  @Column({ name: 'is_seen', default: false })
  isSeen: boolean;
  @ManyToOne(
    () => AccountEntity,
    (accountReceiver) => accountReceiver.notificationReceiver,
    {
      orphanedRowAction: 'delete',
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'receiver' })
  accountReceiver: AccountEntity;
}
