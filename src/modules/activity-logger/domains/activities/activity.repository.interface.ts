import { Activity } from './activity';
export interface IActivityRepository {
  insert(user: Activity): Promise<Activity>;
  update(user: Activity): Promise<Activity>;
  delete(id: string): Promise<boolean>;
  getAll(withDeleted: boolean): Promise<Activity[]>;
  getById(id: string, withDeleted: boolean): Promise<Activity>;
  archive(id: string): Promise<boolean>;
  restore(id: string): Promise<boolean>;
}
