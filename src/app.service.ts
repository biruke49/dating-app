import { Injectable } from '@nestjs/common';
import * as firebase from 'firebase-admin';
import axios from 'axios';
import { SendSmsCommand } from '@notification/usecases/notifications/notification.commands';
@Injectable()
export class AppService {
  private apiKey = process.env.GOOGLE_DISTANCE_API;
  private afroApiKey = process.env.AFRO_API_KEY; // Use environment variables for sensitive data
  private afroSenderId = process.env.AFRO_IDENTIFIER_ID;
  private afroSenderName = process.env.AFRO_SENDER_NAME;

  private afroBaseUrl: string = process.env.AFRO_BASE_URL;
  private afroSpaceBefore: string = process.env.AFRO_SPACE_BEFORE_OTP;
  private afroSpaceAfter: string = process.env.AFRO_SPACE_AFTER_OTP;
  private afroExpiresIn: string = process.env.AFRO_OTP_EXPIRES_IN_SECONDS;
  private afroLength: string = process.env.AFRO_OPT_LENGTH;
  private afroType: string = process.env.AFRO_OTP_TYPE;

  apiUrl = process.env.AFRO_BASE_URL;
  // async sendNotificationLegacy(
  //   token: string,
  //   notificationData: any,
  //   sender?: any,
  //   type?: any,
  // ) {
  //   if (token) {
  //     let title = '';
  //     if (sender) {
  //       title = sender.name;
  //     } else if (notificationData.title) {
  //       title = notificationData.title;
  //     }
  //     const message = {
  //       notification: {
  //         title: title,
  //         body: notificationData.body,
  //       },
  //       data: {
  //         receiverId: notificationData.receiverId,
  //         senderId: notificationData.senderId,
  //         createdAt: notificationData.createdAt,
  //         sender: sender ? sender : null,
  //         type: type ? type : null,
  //       },
  //       to: token,
  //     };
  //     try {
  //       const response = await axios.post(
  //         'https://fcm.googleapis.com/fcm/send',
  //         message,
  //         {
  //           headers: {
  //             Authorization: `key=${process.env.FIREBASE_AUTHORIZATION_KEY}`,
  //             'Content-Type': 'application/json',
  //           },
  //         },
  //       );

  //       console.log('Notification Sent', response.data);
  //     } catch (err) {
  //       console.error('Notification Not Sent', err.response);
  //     }
  //   }
  // }
  async sendNotification(
    token: string,
    notificationData: any,
    sender?: any,
    type?: any,
  ) {
    if (token) {
      let title = '';
      if (sender) {
        title = sender.name;
      } else if (notificationData.title) {
        title = notificationData.title;
      }
      const notification = {
        notification: {
          title: title,
          body: notificationData.body,
        },
        data: {
          receiverId: notificationData.receiverId
            ? notificationData.receiverId
            : '',
          senderId: notificationData.senderId ? notificationData.senderId : '',
          createdAt: notificationData.createdAt
            ? JSON.stringify(notificationData.createdAt)
            : '',
          sender: sender ? JSON.stringify(sender) : '',
          type: type ? type : '',
        },
        token: token,
      };
      firebase
        .messaging()
        .send(notification)
        .then((response) => {
          console.log('res', response);
          return response;
        })
        .catch((error) => {
          console.log('err', error);
          return error;
        });
    }
  }
  async sendSms(command: SendSmsCommand) {
    try {
      const response = await axios.post(
        `${this.apiUrl}/send`,
        {
          from: this.afroSenderId,
          sender: this.afroSenderName,
          to: command.phone,
          message: command.message,
        },
        {
          headers: {
            Authorization: `Bearer ${this.afroApiKey}`,
          },
        },
      );
      const responseData = response.data;
      return responseData;
    } catch (error) {
      throw error;
    }
  }
  async sendVerificationCode(phone: string): Promise<any> {
    console.log("🚀 ~ file: app.service.ts:132 ~ AppService ~ sendVerificationCode ~ phone:", phone)
    const prefixMessage = 'Your Verification Code is';

    try {
      const response = await axios.get(`${this.afroBaseUrl}/challenge`, {
        params: {
          from: this.afroSenderId,
          sender: this.afroSenderName,
          to: phone,
          pr: prefixMessage,
          sb: this.afroSpaceBefore,
          sa: this.afroSpaceAfter,
          ttl: this.afroExpiresIn,
          len: this.afroLength,
          t: this.afroType,
        },
        headers: {
          Authorization: `Bearer ${this.afroApiKey}`,
        },
      });

      const responseData = response.data;
      return responseData;
    } catch (error) {
      // Handle errors appropriately
      throw error;
    }
  }
  async verifyOtp(code: string, phone: string): Promise<any> {
    try {
      const response = await axios.get(`${this.afroBaseUrl}/verify`, {
        params: {
          to: phone,
          code: code,
        },
        headers: {
          Authorization: `Bearer ${this.afroApiKey}`,
        },
      });

      const responseData = response.data;
      return responseData;
    } catch (error) {
      throw error;
    }
  }
}
