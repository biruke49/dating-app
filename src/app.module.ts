import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from '../db/data-source';
import { UserModule } from '@user/user.module';
import { ClassificationModule } from '@classification/classification.module';
import { PublicationModule } from '@publication/publication.module';
import { NotificationModule } from '@notification/notification.module';
import { AccountModule } from '@account/account.module';
import { ActivityModule } from '@activity-logger/activity-logger.module';
import {
  FileManagerModule,
  FileManagerService,
} from '@libs/common/file-manager';
import { PaymentModule } from '@libs/payment/payment.module';
import { ServiceModule } from '@service/service.module';
import { ConfigurationsModule } from '@configurations/configurations.module';
import { FaqModule } from 'modules/faq/faq.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CronService } from 'cron.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ConfigModule } from '@nestjs/config';
import * as dotenv from 'dotenv';
import { CustomerModule } from '@customer/customer.module';

dotenv.config({ path: '.env' });
@Module({
  imports: [
    EventEmitterModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot(dataSourceOptions),
    UserModule,
    ClassificationModule,
    PublicationModule,
    NotificationModule,
    AccountModule,
    ActivityModule,
    FileManagerModule,
    PaymentModule,
    ServiceModule,
    ConfigurationsModule,
    ScheduleModule.forRoot(),
    FaqModule,
    CustomerModule,
  ],
  controllers: [AppController],
  providers: [FileManagerService, AppService, CronService],
})
export class AppModule {}
