interface Role {
  name: string;
  key: string;
  protected?: boolean;
}
interface Permission {
  name: string;
  key: string;
}
export const Roles: Role[] = [
  {
    name: 'Super Admin',
    key: 'super_admin',
    protected: true,
  },
  {
    name: 'Admin',
    key: 'admin',
  },
  {
    name: 'Finance',
    key: 'finance',
  },
  {
    name: 'CRM and Operator',
    key: 'operator',
  },
  {
    name: 'Corporate',
    key: 'corporate',
    protected: true,
  },
  {
    name: 'School',
    key: 'school',
    protected: true,
  },
  {
    name: 'Parent',
    key: 'parent',
    protected: true,
  },
  {
    name: 'Passenger',
    key: 'passenger',
    protected: true,
  },
  {
    name: 'Driver',
    key: 'driver',
    protected: true,
  },

  {
    name: 'Owner',
    key: 'owner',
    protected: true,
  },
];
export const Permissions: Permission[] = [
  {
    name: 'Manage Roles',
    key: 'manage-roles',
  },
  {
    name: 'Manage Permissions',
    key: 'manage-permissions',
  },
  {
    name: 'Manage Account Roles',
    key: 'manage-account-roles',
  },
  {
    name: 'Manage Account Permissions',
    key: 'manage-account-permissions',
  },
  {
    name: 'Import Internal Users',
    key: 'import-users',
  },
  {
    name: 'Manage Users',
    key: 'manage-users',
  },
  {
    name: 'Activate or Block Users',
    key: 'activate-or-block-users',
  },
  {
    name: 'Manage Parents',
    key: 'manage-parents',
  },
  {
    name: 'Activate or Block Parents',
    key: 'activate-or-block-parents',
  },
  {
    name: 'Manage Groups',
    key: 'manage-groups',
  },

  {
    name: 'Activate or Block Groups',
    key: 'activate-or-block-groups',
  },
  {
    name: 'Manage Groups Assignment',
    key: 'manage-group-assignment',
  },
  {
    name: 'Activate or Block Group Assignment',
    key: 'activate-or-block-group-assignment',
  },
  {
    name: 'Manage Categories',
    key: 'manage-categories',
  },
  {
    name: 'Manage Cities',
    key: 'manage-cities',
  },
  {
    name: 'Manage Bus Stops',
    key: 'manage-bus-stops',
  },
  {
    name: 'Manage Routes',
    key: 'manage-routes',
  },
  {
    name: 'Manage Route Stations',
    key: 'manage-route-station',
  },
  {
    name: 'Manage Route Price and Provider Commission',
    key: 'manage-route-prices',
  },
  {
    name: 'Manage Schools',
    key: 'manage-schools',
  },
  {
    name: 'Activate and Block Schools',
    key: 'activate-or-block-schools',
  },
  {
    name: 'Manage Vehicle Owners',
    key: 'manage-owners',
  },
  {
    name: 'Activate and Block Vehicle Owners',
    key: 'activate-or-block-owner',
  },
  {
    name: 'Update Vehicle Owner Documents',
    key: 'update-owner-document',
  },
  {
    name: 'Manage Vehicles',
    key: 'manage-vehicles',
  },
  {
    name: 'Activate and Block Vehicle',
    key: 'activate-or-block-vehicle',
  },
  {
    name: 'Update Vehicle Documents',
    key: 'update-vehicle-document',
  },
  {
    name: 'Manage Providers',
    key: 'manage-drivers',
  },
  {
    name: 'Manage Providers Bank Account',
    key: 'manage-bank-account',
  },
  {
    name: 'Reassign Vehicles',
    key: 're-assign-vehicle',
  },
  {
    name: 'Activate and Block Driver',
    key: 'activate-or-block-driver',
  },
  {
    name: 'Update Providers Document',
    key: 'update-provider-document',
  },
  {
    name: 'Manage Providers Fee',
    key: 'manage-driver-fee',
  },
  {
    name: 'Manage Passengers',
    key: 'manage-passengers',
  },
  {
    name: 'Activate and Block Passenger',
    key: 'activate-or-block-passenger',
  },
  {
    name: 'Manage Corporates',
    key: 'manage-corporates',
  },
  {
    name: 'Activate and Block Corporate',
    key: 'activate-or-block-corporate',
  },
  {
    name: 'Create Corporate Passenger',
    key: 'create-corporate-passenger',
  },
  {
    name: 'Remove Employee from Corporate',
    key: 'remove-employee-from-corporate',
  },
  {
    name: 'Add Employee to Corporate',
    key: 'add-employee-to-corporate',
  },
  {
    name: 'Import Corporate Employees',
    key: 'import-corporate-employees',
  },
  {
    name: 'Manage Assignments',
    key: 'assignments/manage-assignments',
  },
  {
    name: 'Start Assignment',
    key: 'assignments/start-assignment',
  },
  {
    name: 'Complete Assignment',
    key: 'assignments/complete-assignment',
  },
  {
    name: 'Cancel Assignment',
    key: 'assignments/cancel-assignment',
  },
  {
    name: 'Manage Driver Payments',
    key: 'assignments/pay-for-driver-assignment',
  },
  {
    name: 'Manage Corporate Wallet',
    key: 'corporate-wallets/manage-corporate-wallet',
  },
  {
    name: 'Manage Corporate Transactions',
    key: 'manage-corporate-transaction',
  },
  {
    name: 'Transfer Balance to Corporate Employees',
    key: 'transfer-wallet-to-corporate-employees',
  },
  {
    name: 'Manage Booking',
    key: 'manage-booking',
  },
  {
    name: 'Manage Notification',
    key: 'manage-notification',
  },
  {
    name: 'Manage Services',
    key: 'manage-service',
  },
  {
    name: 'Manage News',
    key: 'manage-news',
  },
  {
    name: 'Manage Faq',
    key: 'manage-faq',
  },
  {
    name: 'Manage Feedback',
    key: 'manage-feedback',
  },
  {
    name: 'Manage Review',
    key: 'manage-review',
  },
];
