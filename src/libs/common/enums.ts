export enum StartTime {
  Auto = 'now',
  NextMonth = 'nextMonth',
  NextYear = 'nextYear',
  HaveNotDecided = 'haveNotDecided',
}
export enum Gender {
  Male = 'male',
  Female = 'female',
}
export enum CredentialType {
  User = 'user',
  Customer = 'customer',
}
export enum PaymentMethod {
  Manual = 'Kabba',
  Telebirr = 'Telebirr',
  Chapa = 'Chapa',
}
export enum PaymentStatus {
  Pending = '1',
  Success = '2',
  Timeout = '3',
  Cancelled = '4',
  Failed = '5',
  Error = '-1',
}
export enum ChapaPaymentStatus {
  Pending = 'pending',
  Success = 'success',
  Timeout = 'timeout',
  Cancelled = 'cancelled',
  Failed = 'failed',
  Error = 'error',
}

export enum DateOfWeek {
  MONDAY = 'Mon',
  TUESDAY = 'Tue',
  WEDNESDAY = 'Wed',
  THURSDAY = 'Thu',
  FRIDAY = 'Fri',
  SATURDAY = 'Sat',
  SUNDAY = 'Sun',
}
export enum NotificationMethod {
  Notification = 'notification',
  Sms = 'sms',
  Both = 'both',
}
export enum ChapaSplitType {
  Percentage = 'percentage',
  Flat = 'flat'
}
export enum VisibilitySettings {
  Private = 'Private',
  Following = 'Following',
  Public = 'Public',
}

export enum UserStatus {
  Active = 'active',
  Inactive = 'inactive',
}