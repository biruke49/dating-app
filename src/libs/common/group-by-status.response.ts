import { ApiProperty } from '@nestjs/swagger';

export class GroupByStatusResponse {
  @ApiProperty()
  status: string;
  @ApiProperty()
  count: number;
}
